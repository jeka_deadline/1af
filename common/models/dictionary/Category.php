<?php

namespace common\models\dictionary;

use Yii;

/**
 * This is the model class for table "{{%dictionary_categories}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $display_order
 * @property integer $active
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dictionary_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }
}
