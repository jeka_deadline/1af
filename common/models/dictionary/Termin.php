<?php

namespace common\models\dictionary;

use Yii;

/**
 * This is the model class for table "{{%dictionary_termins}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $text
 * @property integer $display_order
 * @property integer $active
 */
class Termin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dictionary_termins}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'text'], 'required'],
            [['category_id', 'display_order', 'active'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'text' => 'Text',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }
}
