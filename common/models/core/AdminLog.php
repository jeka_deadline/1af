<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_admin_log}}".
 *
 * @property integer $id
 * @property string $description
 * @property string $route
 * @property string $date
 * @property integer $user_id
 */
class AdminLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_admin_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['route', 'user_id'], 'required'],
            [['date'], 'safe'],
            [['user_id'], 'integer'],
            [['route'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'route' => 'Route',
            'date' => 'Date',
            'user_id' => 'User ID',
        ];
    }
}
