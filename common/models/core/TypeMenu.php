<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_type_menu}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property integer $display_order
 * @property integer $active
 */
class TypeMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_type_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'title'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['code', 'title'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'title' => 'Title',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }
}
