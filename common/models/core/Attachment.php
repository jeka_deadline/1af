<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_attachments}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $display_order
 * @property string $file
 */
class Attachment extends \yii\db\ActiveRecord
{
    private static $filePath = 'files/core/attachment';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_attachments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'file'], 'required'],
            [['description'], 'string'],
            [['display_order'], 'integer'],
            [['title', 'file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'file' => 'File',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }
}
