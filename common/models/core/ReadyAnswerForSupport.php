<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_ready_answers_for_support}}".
 *
 * @property integer $id
 * @property string $header
 * @property string $text
 * @property integer $display_order
 */
class ReadyAnswerForSupport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_ready_answers_for_support}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['header', 'text'], 'required'],
            [['text'], 'string'],
            [['display_order'], 'integer'],
            [['header'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'header' => 'Header',
            'text' => 'Text',
            'display_order' => 'Display Order',
        ];
    }
}
