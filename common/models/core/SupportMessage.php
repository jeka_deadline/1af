<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_support_messages}}".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $image
 * @property integer $is_new
 * @property string $date
 * @property string $answer
 */
class SupportMessage extends \yii\db\ActiveRecord
{

    public static $filePath = 'files/core/support';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_support_messages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'name', 'email', 'text'], 'required'],
            [['subject_id', 'is_new'], 'integer'],
            [['text', 'answer'], 'string'],
            [['date'], 'safe'],
            [['name', 'email'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => SupportSubject::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_id' => 'Subject ID',
            'name' => 'Name',
            'email' => 'Email',
            'text' => 'Text',
            'image' => 'Image',
            'is_new' => 'Is New',
            'date' => 'Date',
            'answer' => 'Answer',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }
}
