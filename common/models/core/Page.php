<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_pages}}".
 *
 * @property integer $id
 * @property string $route
 * @property string $header
 * @property string $menu_class
 * @property integer $display_order
 * @property integer $active
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route', 'header'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['route', 'header', 'menu_class'], 'string', 'max' => 255],
            [['route'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route' => 'Route',
            'header' => 'Header',
            'menu_class' => 'Menu Class',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }
}
