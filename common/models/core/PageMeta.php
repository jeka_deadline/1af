<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_pages_meta}}".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $model_class
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */
class PageMeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_pages_meta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'model_class'], 'required'],
            [['owner_id'], 'integer'],
            [['meta_description', 'meta_keywords'], 'string'],
            [['model_class', 'meta_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'model_class' => 'Model Class',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }
}
