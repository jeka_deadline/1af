<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_links_menu_page}}".
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $type_id
 * @property string $type_model
 */
class LinksMenuPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_links_menu_page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'type_id', 'type_model'], 'required'],
            [['page_id', 'type_id'], 'integer'],
            [['type_model'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeMenu::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'type_id' => 'Type ID',
            'type_model' => 'Type Model',
        ];
    }
}
