<?php

namespace common\models\raffle;

use Yii;

/**
 * This is the model class for table "{{%raffle_prizes}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property integer $display_order
 * @property integer $active
 */
class Prize extends \yii\db\ActiveRecord
{

    public static $filePath = 'files/raffle/prizes';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%raffle_prizes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }
}
