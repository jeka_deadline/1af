<?php

namespace common\models\raffle;

use Yii;
use common\models\user\User;

/**
 * This is the model class for table "{{%raffle_check}}".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $user_id
 * @property string $number
 * @property string $check_date
 * @property string $check_image
 * @property string $register_date
 * @property integer $status
 * @property string $reason_rejection
 * @property integer $prize_id
 * @property string $winner_date
 * @property string $user_ip
 * @property string $image_hash
 * @property string $hash_id
 * @property integer $send_status_id
 * @property string $send_number
 */
class Check extends \yii\db\ActiveRecord
{
    const STATUS_PROCESSING = 0;
    const STATUS_REJECT = 1;
    const STATUS_ACCEPT = 2;

    const BACKPACK_PRIZE_ID = 1;
    const TABLET_PRIZE_ID = 2;

    public static $logFile = '/../../../log-winner.txt';

    public static $filePath = 'files/checks';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%raffle_check}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'user_id', 'number'], 'required'],
            [['store_id', 'user_id', 'status', 'prize_id', 'send_status_id'], 'integer'],
            [['check_date', 'register_date', 'winner_date'], 'safe'],
            [['check_image', 'reason_rejection'], 'string'],
            [['number', 'send_number'], 'string', 'max' => 50],
            [['user_ip'], 'string', 'max' => 15],
            [['image_hash', 'hash_id'], 'string', 'max' => 32],
            [['prize_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prize::className(), 'targetAttribute' => ['prize_id' => 'id']],
            [['send_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => SendStatus::className(), 'targetAttribute' => ['send_status_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['store_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_id' => 'Store ID',
            'user_id' => 'User ID',
            'number' => 'Number',
            'check_date' => 'Check Date',
            'check_image' => 'Check Image',
            'register_date' => 'Register Date',
            'status' => 'Status',
            'reason_rejection' => 'Reason Rejection',
            'prize_id' => 'Prize ID',
            'winner_date' => 'Winner Date',
            'user_ip' => 'User Ip',
            'image_hash' => 'Image Hash',
            'hash_id' => 'Hash ID',
            'send_status_id' => 'Send Status ID',
            'send_number' => 'Send Number',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }

    public static function getLogFile()
    {
        return __DIR__.self::$logFile;
    }

    public static function initLogFile()
    {
        $logFile = self::getLogFile();

        if (!file_exists($logFile)) {
            touch($logFile, 0777);
        }
    }
}
