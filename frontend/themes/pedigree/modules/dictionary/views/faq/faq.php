<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Вопросы-ответы';
?>
<?= $this->render('@app/views/layouts/mobile-menu'); ?>

<div class="main-wrapper">

    <?= $this->render('@app/views/layouts/header-full'); ?>

    <div class="b-faq">
      <div class="container">
        <h1 class="b-title">FAQ</h1>
        <div class="clearfix">
          <div class="panel panel--faq">
            <ul class="tabs clearfix">

                <?php foreach ($contentFaq as $index => $tab): ?>
                    <?php
                        $options = [];

                        if (!$index) {
                            Html::addCssClass($options, 'active');
                        }
                    ?>

                    <?= Html::beginTag('li', $options); ?>

                        <a href="#"><?= $tab[ 'category-name' ]; ?></a>

                    <?= Html::endTag('li'); ?>

                <?php endforeach; ?>

            </ul>
            <div class="content">

                <?php foreach ($contentFaq as $index => $tab): ?>
                    <?php
                        $options = ['class' => 'tab'];

                        if (!$index) {
                            Html::addCssClass($options, 'active');
                        }
                    ?>

                    <?= Html::beginTag('div', $options); ?>

                        <?php if ($tab[ 'items' ]) : ?>

                            <ol>

                              <?php foreach ($tab[ 'items' ] as $index => $itemQuestion): ?>

                                  <li>
                                      <span class="faq-link"><?= $index + 1; ?>)  <?= $itemQuestion->name; ?></span>
                                      <div class="faq-content"><?= $itemQuestion->text; ?></div>
                                  </li>

                              <?php endforeach; ?>

                            </ol>

                        <?php endif; ?>

                    <?= Html::endTag('div'); ?>

                <?php endforeach; ?>

            </div>
          </div>
          <div class="panel panel--ask">
            <div class="inner">
              <h2>не нашли ответа на свой вопрос?</h2>
              <p>Вы можете задать его здесь</p>
              <a class="btn-ask modal-open" href="" data-url="<?= Url::toRoute(['/core/support-ajax/support-message']); ?>">задать вопрос</a>
            </div>
          </div>
        </div>
      </div>
    </div>

<?= $this->render('@app/views/layouts/footer'); ?>