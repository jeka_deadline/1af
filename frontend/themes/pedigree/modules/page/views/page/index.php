<?= $this->render('@app/views/layouts/mobile-menu'); ?>

<div class="main-wrapper">

    <?= $this->render('@app/views/layouts/header-full'); ?>

    <?= $page->content; ?>

    <?= $this->render('@app/views/layouts/footer'); ?>

</div>