<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'ajax-modal-form content'
    ],
    'fieldConfig' => [
        'options' => [
            'tag' => FALSE
        ],
        'template' => '{input}'
    ],
    'action' => ['/raffle/check-ajax/check-user-age'],
]); ?>

    <h2 class="title">Проверка возраста</h2>
    <div class="heading">Как ответственный производитель, мы&nbsp;должны проверить ваш возраст, чтобы убедиться, что мы&nbsp;придерживаемся наших обязательств на&nbsp;рынках и&nbsp;перед своим потребителем</div>

    <div class="inputs">
        <label>Дата рождения</label>

        <?= $form->field($model, 'dayBirth')->dropDownList($model->getListDays(), ['class' => 'select-2 styled', 'prompt' => '--']); ?>

        <?= $form->field($model, 'monthBirth')->dropDownList($model->getListMonths(), ['class' => 'select-2 styled', 'prompt' => 'Не выбрано']); ?>

        <?= $form->field($model, 'yearBirth')->dropDownList($model->getListYears(), ['class' => 'select-2 styled', 'prompt' => '----']); ?>

    </div>

    <?= Html::submitButton('Продолжить', ['class' => 'submit-btn button-2']); ?>

<?php ActiveForm::end(); ?>