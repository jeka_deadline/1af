<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/raffle/check-ajax/register-check-step2', 'staticPage' => TRUE],
    'options' => [
        'class' => (Yii::$app->user->isGuest) ? 'd-table ajax-modal-form form-modal' : 'd-table ajax-modal-form',
        'enctype' => 'multipart/form-data',
        'data-url' => ['/user/security/get-guest-form'],
    ],
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

  <div class="d-table-row">
    <div class="inputs d-table-cell">
      <div class="inputs-row align-middle">
          <label class="input-label"><span><?= $model->getAttributeLabel('numberCheck'); ?>&nbsp;<span class="button-4">?</span></span></label>

          <?= $form->field($model, 'numberCheck', [
              'options' => [
                  'class' => 'input-1 input'
              ],
              'template' => '{input}{error}',
          ])->label(FALSE); ?>

      </div>
      <div class="inputs-row align-middle">
          <label class="input-label"><span><?= $model->getAttributeLabel('dateCheck'); ?>&nbsp;<span class="button-4">?</span></span></label>

          <?= $form->field($model, 'dateCheck', [
              'options' => [
                  'class' => 'input-1 input'
              ],
              'template' => '{input}{error}',
          ])->textInput(['placeholder' => date('d/m/Y H:i:s'), 'class' => 'datetime-input'])->label(FALSE); ?>

      </div>

      <?= $form->field($model, 'imageCheck', [
          'options' => [
              'class' => 'inputs-row align-middle'
          ],
          'template' => '<label class="input-label"><span>' . $model->getAttributeLabel('imageCheck') . '</span></label>{input}',
          'enableAjaxValidation' => FALSE,
      ])->fileInput(['class' => 'file-1 styled', 'data' => ['browse' => 'Обзор'], 'accept' => '.png, .jpeg, .jpg, .bmp, .pdf'])->label(FALSE); ?>

    </div>

    <div class="captcha d-table-cell">

        <?= $form->field($model, 'captcha', [
            'options' => [
                'tag' => FALSE,
            ],
            'enableAjaxValidation' => FALSE,
        ])->widget(\yii\captcha\Captcha::className(), [
            'captchaAction' => '/core/index/captcha',
            'template' => '<div class="image">{image}</div><label>' . $model->getAttributeLabel('captcha') . '</label><div class="input-1 input">{input}</div>',
            'options' => [
                'class' => NULL,
            ],
        ])->label(FALSE); ?>

    </div>
  </div>
  <div class="d-table-row bottom">
    <div class="d-table-cell left">
        <div class="inputs-row field-registercheckstep2form-agreerule required">
            <?= $form->field($model, 'agreeRule', [
                'options' => [
                    'class' => 'inputs-row',
                ],
                'checkboxTemplate' => '<label class="agreement clearfix">{input}{error}<span>Я согласен с <a href="#">правилами акции</a>, с <a href="#">пользовательским соглашением</a> и с <a href="#">положением о конфиденциальности</a></span></label>',
            ])->checkbox(['class' => 'checkbox-2 styled']); ?>
        </div>
    </div>
    <div class="d-table-cell right">
        <button type="submit" class="submit-btn button-2">Отправить</button>

    </div>
  </div>

<?php ActiveForm::end(); ?>