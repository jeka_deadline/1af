<div id="modal_reg_1" class="modal modal-1">
    <a href="#" class="modal-close">&nbsp;</a>

    <?= $this->render('register-check-step1-form', [
        'model' => $model,
    ]); ?>

</div>