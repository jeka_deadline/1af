<div id="modal_reg_2" class="modal modal-1">
    <a href="#" class="modal-close">&nbsp;</a>

    <?= $this->render('register-check-step2-form', [
        'model' => $model,
    ]); ?>

</div>