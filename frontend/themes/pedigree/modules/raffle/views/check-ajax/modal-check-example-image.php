<div id="modal_check_help" class="secondary-modal">
    <div class="modal modal-1">
        <a href="#" class="modal-close">&nbsp;</a>
        <div class="content">
            <h2 class="title">Как найти номер чека, дату и время?</h2>
            <div class="check"><img src="<?= $this->theme->getUrl('style/images/check-2.jpg'); ?>" alt=""></div>
            <ul class="clearfix">
                <li><span>1</span>Дата с чека</li>
                <li><span>2</span>Время с чека</li>
                <li><span>3</span>Номер чека</li>
            </ul>
            <a href="#" class="close-btn button-2">OK</a>
        </div>
    </div>
</div>