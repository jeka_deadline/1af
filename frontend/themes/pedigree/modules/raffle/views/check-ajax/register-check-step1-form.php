<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>

<?php $form = ActiveForm::begin([
    'action' => ['/raffle/check-ajax/register-check-step1'],
    'options' => [
        'class' => 'ajax-modal-form content',
        'data' => ['url' => ['/raffle/check-ajax/send-sms']]
    ],
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

    <h2 class="title">Регистрация чека, шаг 1 из 2</h2>
    <div class="heading">Заполните, пожалуйста, поля, представленные ниже</div>
    <div class="inputs">
        <div class="inputs-group">
            <div class="inputs-row align-middle clearfix">
                <label class="input-label"><span><?= $model->getAttributeLabel('phone'); ?></span></label>
                <?= $form->field($model, 'phone', [
                    'options' => [
                        'class' => 'input-1 input',
                    ],
                    'template' => '{input}{error}',
                ])->widget(MaskedInput::className(), [
                    'class' => 'phone-input',
                    'mask' => '+7 (999) 999-9999',
                ])->label(FALSE); ?>
            </div>
            <div class="inputs-row clearfix">
                <label class="checkbox-label">
                    <?= $form->field($model, 'isRegister', [
                        'options' => [
                            'tag' => FALSE
                        ],
                        'checkboxTemplate' => '{input}'
                    ])->checkbox([
                        'class' => 'js-is-register checkbox-2 styled'
                    ])->label(FALSE); ?>
                    <?= $model->getAttributeLabel('isRegister'); ?>
                </label>
            </div>
            <?php
                $options = [
                    'class' => 'inputs-row align-middle clearfix',
                ];

                if (!$model->isRegister) {
                    Html::addCssClass($options, 'hidden');
                }
            ?>

            <?= Html::beginTag('div', $options); ?>

                <label class="input-label"><span><?= $model->getAttributeLabel('password'); ?></span></label>
                <?= $form->field($model, 'password', [
                    'options' => [
                        'class' => 'input-1 input'
                    ],
                    'template' => '{input}{error}',
                ])->passwordInput(['id' => 'input-login-password'])->label(FALSE); ?>

                <?php
                $options = [
                    'class' => 'problem replace-modal-form',
                    'id'    => 'btn-reset-password',
                    'data'  => [
                        'url' => Url::toRoute(['/user/security/reset-password']),
                    ],
                ];

                if (!$model->isRegister) {
                    Html::addCssClass($options, 'hidden');
                }

                echo Html::a('Забыли пароль?', '#', $options);
                ?>

            <?= Html::endTag('div'); ?>

        </div>

        <?= Html::beginTag('div', ['class' => ($model->isRegister || $model->getErrors('phone')) ? 'hidden inputs-group' : 'inputs-group', 'id' => 'block-sms']); ?>

            <div class="inputs-row clearfix">
                <label class="checkbox-label">
                    <?= $form->field($model, 'isSmsSend', [
                        'options' => [
                            'tag' => FALSE
                        ],
                        'checkboxTemplate' => '{input}'
                    ])->checkbox([
                        'class' => 'checkbox-2 styled',
                        'id' => 'js-checkbox-is-send-sms',
                    ])->label(FALSE); ?>
                    <span data-label="<?= $model->getDataLabelForStatusSms(); ?>" id='label-status-sms'><?= $model->getAttributeLabelForStatusSms(); ?></span>

                </label>
            </div>

            <?php
                $optionsWrapper = [
                    'class' => 'inputs-row align-middle clearfix hidden',
                    'id' => 'block-input-sms-code',
                ];

                if ($model->isSmsSend) {
                    Html::removeCssClass($optionsWrapper, 'hidden');
                }
            ?>

            <?= Html::beginTag('div', $optionsWrapper); ?>
                <label class="input-label"><span><?= $model->getAttributeLabel('smsCode'); ?></span></label>
                <?= $form->field($model, 'smsCode', [
                    'options' => [
                        'class' => 'input-1 input'
                    ],
                    'template' => '{input}{error}',
                ])->label(FALSE); ?>
                <a href="#" id="resend-sms" class="problem">Не пришел код?</a><div id="timer"></div>

            <?= Html::endTag('div'); ?>

        <?= Html::endTag('div'); ?>
        <?= Html::beginTag('div', ['class' => ($model->isValidCode() && $model->isSmsSend) ? 'inputs-group' : 'hidden inputs-group', 'id' => 'block-register']); ?>

            <div class="inputs-row align-middle clearfix">
                <label class="input-label"><span><?= $model->getAttributeLabel('name'); ?></span></label>
                <?= $form->field($model, 'name', [
                    'options' => [
                        'class' => 'input-1 input wide'
                    ],
                    'template' => '{input}{error}',
                ])->label(FALSE); ?>
            </div>
            <div class="inputs-row align-middle clearfix">
                <label class="input-label"><span><?= $model->getAttributeLabel('permanentPassword'); ?></span></label>
                <?= $form->field($model, 'permanentPassword', [
                    'options' => [
                        'class' => 'input-1 input wide'
                    ],
                    'template' => '{input}{error}',
                ])->passwordInput()->label(FALSE); ?>
            </div>

        <?= Html::endTag('div'); ?>

    </div>
    <div class="buttons">

        <?= Html::submitButton('Далее', ['class' => 'button-2']); ?>

    </div>

<?php ActiveForm::end(); ?>