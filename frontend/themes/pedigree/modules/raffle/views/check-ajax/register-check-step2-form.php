<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/raffle/check-ajax/register-check-step2'],
    'options' => [
        'class' => 'content',
        'enctype'=>'multipart/form-data',
    ],
    'id' => 'register-form-step2',
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

    <h2 class="title">Регистрация чека, шаг 2 из 2</h2>
    <div class="heading">Заполните, пожалуйста, поля, представленные ниже</div>
    <div class="inputs">
        <div class="inputs-group">

            <?= $form->field($model, 'storeId', [
                'options' => [
                    'class' => 'inputs-row clearfix'
                ],
                'template' => '<label class="input-label"><span>' . $model->getAttributeLabel('storeId') . '</span></label>{beginWrapper}{input}{endWrapper}',
                'radioTemplate' => '<label>{input}</label>',
                'wrapperOptions' => [
                    'class' => 'network clearfix',
                ],
            ])->radioList($model->getStores(), [
                'item' => function($index, $label, $name, $checked, $value) use ($model){
                    return '<label>' . Html::radio($name, $checked, ['value' => $value, 'class' => 'radio-1 styled']) . '<span class="logo">' . Html::img($label->getFullImagePath()) . '</span></label>';
                }
            ])->label(FALSE); ?>

        </div>
        <div class="inputs-group">
            <div class="check clearfix">
                <a href="#modal_check_help" class="help-btn secondary-modal-open" data-url="<?= Url::toRoute(['/raffle/check-ajax/get-check-example-image']); ?>"><img src="<?= $this->theme->getUrl('style/images/check-1.png'); ?>" alt=""></a>
                <div class="inputs-row align-middle">
                    <label class="input-label"><span><?= $model->getAttributeLabel('numberCheck'); ?>&nbsp;<span href="#modal_check_help" class="button-4 secondary-modal-open" data-url="<?= Url::toRoute(['/raffle/check-ajax/get-check-example-image']); ?>">?</span></span></label>

                    <?= $form->field($model, 'numberCheck', [
                        'options' => [
                            'class' => 'input-1 input wide'
                        ],
                        'template' => '{input}{error}',
                    ])->label(FALSE); ?>

                </div>
                <div class="inputs-row align-middle">
                    <label class="input-label"><span><?= $model->getAttributeLabel('dateCheck'); ?>&nbsp;<span href="#modal_check_help" class="button-4 secondary-modal-open" data-url="<?= Url::toRoute(['/raffle/check-ajax/get-check-example-image']); ?>">?</span></span></label>

                    <?= $form->field($model, 'dateCheck', [
                        'options' => [
                            'class' => 'input-1 input wide'
                        ],
                        'template' => '{input}{error}',
                    ])->textInput(['placeholder' => date('d/m/Y H:i:s'), 'class' => 'datetime-input'])->label(FALSE); ?>

                </div>
            </div>

            <?= $form->field($model, 'imageCheck', [
                'options' => [
                    'class' => 'inputs-row align-middle clearfix'
                ],
                'template' => '<label class="input-label"><span>' . $model->getAttributeLabel('imageCheck') . '</span></label>{input}',
                'enableAjaxValidation' => FALSE,
            ])->fileInput(['class' => 'file-1 styled', 'data' => ['browse' => 'Обзор'], 'accept' => '.png, .jpeg, .jpg, .bmp, .pdf'])->label(FALSE); ?>

        </div>

        <div class="inputs-group">

            <?= $form->field($model, 'captcha', [
                'options' => [
                    'class' => 'inputs-row captcha'
                ],
                'enableAjaxValidation' => FALSE,
            ])->widget(\yii\captcha\Captcha::className(), [
                'captchaAction' => '/core/index/captcha',
                'template' => '<div class="heading">' . $model->getAttributeLabel('captcha') . '</div><div class="clearfix"><div class="image">{image}</div><div class="input-1 input wide">{input}</div></div>',
                'options' => [
                    'class' => NULL,
                ],
            ])->label(FALSE); ?>

            <?= $form->field($model, 'agreeRule', [
                'options' => [
                    'class' => 'inputs-row',
                ],
                'checkboxTemplate' => '<label class="agreement clearfix">{input}<span>Я согласен с <a href="#">правилами акции</a>,<br>с <a href="#">пользовательским соглашением</a><br>и с <a href="#">положением о конфиденциальности</a></span></label>',
            ])->checkbox(['class' => 'checkbox-1 styled']); ?>

        </div>

    </div>

    <div class="buttons">

        <?= Html::submitButton('Отправить', ['class' => 'button-2']); ?>

    </div>

<?php ActiveForm::end(); ?>