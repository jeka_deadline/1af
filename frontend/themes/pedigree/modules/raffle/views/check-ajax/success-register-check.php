<?php
use frontend\modules\core\models\Helper;
use yii\helpers\Url;
?>
<div class="content">
    <h2 class="title">Чек зарегистрирован</h2>
    <div class="heading">
        <p>Спасибо!</p>
        <p>Ваш чек успешно зарегистрирован.</p>
        <p>Результаты розыгрыша будут доступны в разделе <a href="<?= Url::toRoute(['/raffle/check/winners']); ?>">Победители</a> на сайте не позднее 1 сентября 2017 года. </p>
    </div>
    <a href="#" class="close-btn button-2">Закрыть</a>
</div>