<div id="modal_birth" class="modal modal-1">
    <a href="#" class="modal-close">&nbsp;</a>

    <?= $this->render('check-user-age-form', [
        'model' => $model,
    ]); ?>

</div>