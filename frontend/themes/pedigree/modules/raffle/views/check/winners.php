<?php
use yii\grid\GridView;
use yii\helpers\Html;
use frontend\modules\core\widgets\PedigreeLinkPager\PedigreeLinkPager;
use frontend\assets\WinnerAsset;
use yii\widgets\ActiveForm;
use frontend\modules\user\widgets\UserAccount\UserAccount;

WinnerAsset::register($this);

$this->title = 'Победители';
?>

<?= $this->render('@app/views/layouts/mobile-menu'); ?>

<div class="main-wrapper">

  <?= $this->render('@app/views/layouts/header-full'); ?>

  <?php $form = ActiveForm::begin([
      'action' => ['winners'],
      'method' => 'GET',
  ]); ?>

      <div class="b-winners">
        <div class="container">
          <h1 class="b-title">Победители</h1>
          <div class="search">
            <label>Введите телефон или имя</label>
            <span class="input-2 ico-search"><?= $form->field($searchModel, 'userNameOrPhone', ['options' => ['tag' => FALSE], 'template' => '{input}'])->textInput(['placeholder' => 'Константин Иванов']); ?></span>
          </div>

          <?= GridView::widget([
              'options' => [
                  'class' => 'table scroll-overlay',
              ],
              'tableOptions' => [
                  'class' => NULL,
              ],
              'dataProvider' => $dataProvider,
              'layout' => '{items}',
              'columns' => [
                  [
                      'attribute' => 'userNameOrPhone',
                      'label' => 'Фамилия, имя',
                      'value' => function($model){ return $model->user->full_name;},
                  ],
                  [
                      'attribute' => 'userNameOrPhone',
                      'label' => 'Номер телефона',
                      'value' => function($model){ return $model->user->getHiddenPhone(); },
                  ],
                  [
                      'attribute' => 'prize_id',
                      'header' => 'Приз' . Html::activeDropDownList($searchModel, 'prize_id', $listPrizes, ['class' => 'select-1 styled js-action-select']),
                      'value' => function($model){ return $model->prize->name;},
                  ],
                  [
                      'attribute' => 'winner_date',
                      'header' => 'Дата' . Html::activeDropDownList($searchModel, 'winner_date', $listDates, ['class' => 'select-1 styled js-action-select']),
                      'value' => function($model){ return Yii::$app->formatter->asDate($model->winner_date, 'php:Y/m/d');},
                  ],
              ],
          ]); ?>

          <?= PedigreeLinkPager::widget([
              'pagination' => $dataProvider->pagination,
              'options' => [
                  'class' => 'pagination-1',
              ],
              'prevPageCssClass' => '',
              'prevPageLabel' => '&nbsp;',
              'nextPageCssClass' => '',
              'nextPageLabel' => '&nbsp;',
          ]); ?>

        </div>
      </div>

  <?php ActiveForm::end(); ?>

  <?= $this->render('@app/views/layouts/footer'); ?>
</div>