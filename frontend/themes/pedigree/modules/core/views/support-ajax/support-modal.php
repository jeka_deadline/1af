<div id="modal_ask" class="modal modal-1">
    <a href="#" class="modal-close">&nbsp;</a>

    <?= $this->render('support-form', [
        'model' => $model,
    ]); ?>

</div>