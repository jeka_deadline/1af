<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'ajax-modal-form content'],
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

    <h2 class="title">Задать вопрос</h2>
    <div class="inputs clearfix">

        <div class="inputs-row clearfix">
            <label class="input-label"><?= $model->getAttributeLabel('name'); ?></label>

            <?= $form->field($model, 'name', [
                'options' => [
                    'class' => 'input-1 input wide'
                ],
                'template' => ($model->getErrors('name')) ? '{input}{error}' : '{input}',
            ])->label(FALSE); ?>

        </div>
        <div class="inputs-row clearfix">
            <label class="input-label"><?= $model->getAttributeLabel('email'); ?></label>

            <?= $form->field($model, 'email', [
                'options' => [
                    'class' => 'input-1 input wide'
                ],
                'template' => ($model->getErrors('email')) ? '{input}{error}' : '{input}',
            ])->label(FALSE); ?>

        </div>
        <div class="inputs-row clearfix">
            <label class="input-label"><?= $model->getAttributeLabel('subjectId'); ?></label>

            <?= $form->field($model, 'subjectId', [
                'options' => [
                    'tag' => FALSE,
                ],
                'template' => ($model->getErrors('subjectId')) ? '{input}{error}' : '{input}',
            ])->dropDownList($model->getListSupportSubjects(), ['class' => 'select-2 input wide styled'])->label(FALSE); ?>

        </div>
        <div class="inputs-row clearfix">
            <label class="input-label"><?= $model->getAttributeLabel('text'); ?></label>

            <?= $form->field($model, 'text', [
                'options' => [
                    'class' => 'textarea-1 wide input'
                ],
                'template' => ($model->getErrors('text')) ? '{input}{error}' : '{input}',
            ])->textarea(['rows' => 8])->label(FALSE); ?>
        </div>

        <?= $form->field($model, 'image', [
            'options' => [
                'class' => 'inputs-row clearfix'
            ],
            'template' => '<label class="input-label"><span>' . $model->getAttributeLabel('image') . '</span></label>{input}',
        ])->fileInput(['class' => 'file-1 input wide styled', 'data' => ['browse' => 'Обзор']])->label(FALSE); ?>

    </div>
    <div class="buttons">
        <button type="submit" class="button-2">Отправить</button>
    </div>

<?php ActiveForm::end(); ?>