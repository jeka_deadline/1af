<?php
use yii\helpers\Url;

$this->title = 'Призы';
?>

<?= $this->render('@app/views/layouts/mobile-menu'); ?>

<div class="main-wrapper">

    <?= $this->render('@app/views/layouts/header-full'); ?>

    <div class="b-homescreen">
        <img class="promo-hires" src="<?= $this->theme->getUrl('style/images/homescreen-promo-1.png'); ?>" alt="">
        <img class="promo-lowres" src="<?= $this->theme->getUrl('style/images/homescreen-promo-3.png'); ?>" alt="">
    </div>
    <div class="b-steps">
        <div class="container">
            <div class="heading">
              Собаки делают нас лучше. <br>
              <img src="<?= $this->theme->getUrl('style/images/word-1.png'); ?>" alt=""> делать лучшее для них
            </div>
            <div class="items clearfix">
              <div class="item">
                <div class="num"><img src="<?= $this->theme->getUrl('style/images/digit-1-1.png'); ?>" alt=""></div>
                <div class="text">
                  <div class="row-1">Купите от 200&nbsp;Р</div>
                  <div class="row-2">любую продукцию <strong>Pedigree&reg;</strong><br>в сети <img src="<?= $this->theme->getUrl('style/images/lenta-logo-1.png'); ?>" alt=""></div>
                </div>
              </div>
              <div class="item">
                <div class="num"><img src="<?= $this->theme->getUrl('style/images/digit-2-1.png'); ?>" alt=""></div>
                <div class="text">
                  <div class="row-1">Зарегистрируйте чек</div>
                </div>
                <a href="javascript:void(0)" class="button-1 register-btn modal-open" data-url="<?= Url::toRoute(['/raffle/check-ajax/register-check-step1']); ?>">Зарегистрировать чек</a>
              </div>
              <div class="item">
                <div class="num"><img src="<?= $this->theme->getUrl('style/images/digit-3-1.png'); ?>" alt=""></div>
                <div class="text">
                  <div class="row-1">Выиграйте</div>
                  <div class="row-2"><strong>Призы</strong></div>
                </div>
              </div>
            </div>
            <!--
            <div class="heading lowres">
              Собаки делают нас лучше. <br>
              <img src="style/images/word-1.png" alt=""> делать лучше для них
            </div>
            -->
        </div>
    </div>

    <?= $this->render('@app/views/layouts/footer'); ?>
</div>
