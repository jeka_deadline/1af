<?php
use yii\helpers\Url;

$this->title = 'Правила';
?>

<?= $this->render('@app/views/layouts/mobile-menu'); ?>

<div class="main-wrapper">

    <?= $this->render('@app/views/layouts/header-full'); ?>

        <div class="b-rules">
            <div class="container">
              <div class="items clearfix">
                <div class="item">
                  <div class="num"><img src="<?= $this->theme->getUrl('style/images/digit-1-2.png'); ?>" alt=""></div>
                  <div class="text">
                    <div class="row-1"><span class="text-uppercase">Купите любую продукцию</span> Pedigree&reg;</div>
                    <div class="row-2">на сумму от 200 рублей в сети <img src="<?= $this->theme->getUrl('style/images/lenta-logo-2.png'); ?>" alt=""></div>
                  </div>
                </div>
                <div class="item">
                  <div class="num"><img src="<?= $this->theme->getUrl('style/images/digit-2-2.png'); ?>" alt=""></div>
                  <div class="text">
                    <div class="row-1"><span class="text-uppercase">Зарегистрируйте чек</span> на покупку</div>
                  </div>

                  <?= $this->render('@app/modules/raffle/views/check-ajax/form-register-check-page-rules', [
                      'model' => $model,
                  ]); ?>

                </div>
                <div class="item">
                  <div class="num"><img src="<?= $this->theme->getUrl('style/images/digit-3-2.png'); ?>" alt=""></div>
                  <div class="text">
                    <div class="row-1"><span class="text-uppercase">Получите шанс</span> выиграть призы</div>
                  </div>
                </div>
              </div>
              <div class="promo clearfix">
                <div class="item item-1"><img src="<?= $this->theme->getUrl('style/images/rules-promo-3.png'); ?>" alt=""></div>
                <div class="item item-2"><img src="<?= $this->theme->getUrl('style/images/rules-promo-4.png'); ?>" alt=""></div>
              </div>
              <div class="pdf-btn"><a href="javascript:void(0)">Полные правила акции</a></div>
            </div>
        </div>

    <?= $this->render('@app/views/layouts/footer'); ?>

</div>