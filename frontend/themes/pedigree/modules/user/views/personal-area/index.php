<?php
use frontend\modules\user\widgets\UserAddress\UserAddress;
use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\modules\core\widgets\PedigreeLinkPager\PedigreeLinkPager;


$this->title = 'Личный кабинет';
?>

<?= $this->render('@app/views/layouts/mobile-menu'); ?>

<div class="main-wrapper">

    <?= $this->render('@app/views/layouts/header-full'); ?>

  <div class="b-cabinet">
    <div class="container">
      <h1 class="b-title">Личный кабинет</h1>
      <div class="panel">
        <ul class="tabs clearfix">

          <?php $optionsTab1 = [
              'class' => (!Yii::$app->user->identity->isUserWinner() || Yii::$app->request->get('tab') === 'check-prizes') ? 'active' : NULL,
          ]; ?>

          <?= Html::beginTag('li', $optionsTab1); ?><a href="#tab_1">Чеки и призы</a><?= Html::endTag('li'); ?>

          <?php if (Yii::$app->user->identity->isUserWinner()) : ?>

              <?php $optionsTab2 = [
                  'class' => (Yii::$app->request->get('tab') !== 'check-prizes') ? 'active' : NULL,
              ]; ?>

              <?= Html::beginTag('li', $optionsTab2); ?><a href="#tab_2">Доставка призов</a><?= Html::endTag('li'); ?>

          <?php endif; ?>

        </ul>
        <div class="content">

              <?php Html::addCssClass($optionsTab1, 'tab tab--prizes'); ?>

              <?php $optionsTab1[ 'id' ] = 'tab_1'; ?>

              <?= Html::beginTag('div', $optionsTab1); ?>

                <div class="table scroll-overlay">
                  <table>
                      <thead>
                        <tr>
                          <th>Название приза</th>
                          <th>Дата выигриша</th>
                          <th>Номер чека</th>
                          <th>Статус</th>
                        </tr>
                      </thead>
                      <tbody>

                          <?php if ($listChecksPrizesDataProvider->totalCount) : ?>

                              <?= ListView::widget([
                                  'dataProvider' => $listChecksPrizesDataProvider,
                                  'layout' => '{items}',
                                  'emptyText' => '',
                                  'emptyTextOptions' => [
                                      'tag' => FALSE,
                                  ],
                                  'options' => [
                                      'tag' => FALSE,
                                  ],
                                  'itemOptions' => [
                                      'tag' => FALSE,
                                  ],
                                  'itemView' => 'raw-check-prizes',
                              ]); ?>

                        <?php else : ?>

                            <tr><td colspan="4">Ничего не найдено</td></tr>

                        <?php endif; ?>

                      </tbody>
                    </table>
                </div>

              <?= Html::endTag('div'); ?>

              <?php if (Yii::$app->user->identity->isUserWinner()) : ?>

                <?php Html::addCssClass($optionsTab2, 'tab'); ?>

                <?php $optionsTab2[ 'id' ] = 'tab_2'; ?>

                <?= Html::beginTag('div', $optionsTab2); ?>

                    <?= UserAddress::widget(); ?>

                    <div class="status">
                      <h2>Статус доставки призов</h2>
                      <div class="table scroll-overlay">

                          <table>
                              <thead>
                                  <tr>
                                      <th>Название приза</th>
                                      <th>Статус</th>
                                      <th>Номер отслеживания (ШПИ)</th>
                                      <th>&nbsp;</th>
                                  </tr>
                              </thead>
                              <tbody>

                                  <?php if ($listWinnersChecksDataProvider->totalCount) : ?>

                                      <?= ListView::widget([
                                          'dataProvider' => $listWinnersChecksDataProvider,
                                          'layout' => '{items}',
                                          'options' => [
                                              'tag' => FALSE,
                                          ],
                                          'emptyText' => '',
                                          'emptyTextOptions' => [
                                              'tag' => FALSE,
                                          ],
                                          'itemOptions' => [
                                              'tag' => FALSE,
                                          ],
                                          'itemView' => 'raw-winner-check',
                                      ]); ?>

                                <?php else : ?>

                                    <tr><td colspan="4">Ничего не найдено</td></tr>

                                <?php endif; ?>

                              </tbody>
                          </table>
                      </div>
                    </div>

                  <?= Html::endTag('div'); ?>

              <?php endif; ?>

        </div>
      </div>
      <div class="pagination">

          <?= PedigreeLinkPager::widget([
              'pagination' => $listChecksPrizesDataProvider->pagination,
              'options' => [
                  'class' => (Yii::$app->request->get('tab') === 'check-prizes') ? 'pagination-1 active' : 'pagination-1',
                  'id' => 'tab_1_pagination',
              ],
              'prevPageCssClass' => '',
              'prevPageLabel' => '&nbsp;',
              'nextPageCssClass' => '',
              'nextPageLabel' => '&nbsp;',
          ]); ?>
          <?= PedigreeLinkPager::widget([
              'pagination' => $listWinnersChecksDataProvider->pagination,
              'options' => [
                  'class' => (Yii::$app->request->get('tab') !== 'check-prizes') ? 'pagination-1 active' : 'pagination-1',
                  'id' => 'tab_2_pagination',
              ],
              'prevPageCssClass' => '',
              'prevPageLabel' => '&nbsp;',
              'nextPageCssClass' => '',
              'nextPageLabel' => '&nbsp;',
          ]); ?>

    </div>
  </div>
</div>

<?= $this->render('@app/views/layouts/footer'); ?>

</div>