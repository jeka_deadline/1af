<div id="modal_modals" class="modal modal-1">
    <a href="#" class="modal-close">&nbsp;</a>

    <?= $this->render('reset-password-form', [
        'model' => $model
    ]); ?>

</div>