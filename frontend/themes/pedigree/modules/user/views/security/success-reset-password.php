<div id="modal_password" class="modal modal-1">
    <a href="#" class="modal-close">&nbsp;</a>
    <div class="content">
        <h2 class="title">Напоминание пароля</h2>
        <div class="heading">
            <p>Спасибо!</p>
            <p>Временный пароль отправлен на указанный телефон в СМС-сообщении.</p>
        </div>
        <a href="#" class="close-btn button-2">Закрыть</a>
    </div>
</div>