<div id="modal_auth" class="modal modal-1">
    <a href="#" class="modal-close">&nbsp;</a>

    <?= $this->render('login-form', [
        'model' => $model
    ]); ?>

</div>