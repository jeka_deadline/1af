<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'ajax-modal-form content'],
    'action' => ['/user/security/reset-password'],
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

    <h2 class="title">Сброс пароля</h2>
    <div class="inputs clearfix">

        <div class="inputs-row clearfix">
            <label class="input-label"><?= $model->getAttributeLabel('phone'); ?></label>

            <?= $form->field($model, 'phone', [
                'options' => [
                    'class' => 'input-1 input wide'
                ],
                'template' => ($model->getErrors('phone')) ? '{input}{error}' : '{input}',
            ])->textInput(['class' => 'phone-input'])->label(FALSE); ?>

        </div>

    </div>
    <div class="buttons">
        <button type="submit" class="button-2">Сбросить пароль</button>
    </div>

<?php ActiveForm::end(); ?>