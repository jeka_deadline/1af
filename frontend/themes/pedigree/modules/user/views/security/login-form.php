<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [
        'class' => 'ajax-modal-form content'
    ],
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

    <h2 class="title">Авторизация на сайте</h2>
    <div class="inputs clearfix">
        <div class="inputs-row clearfix">
            <label class="input-label"><?= $model->getAttributeLabel('phone'); ?></label>

            <?= $form->field($model, 'phone', [
                'options' => [
                    'class' => 'input-1 input'
                ],
                'template' => '{input}{error}',
            ])->widget(MaskedInput::className(), [
                'mask' => '+7 (999) 999-9999',
            ])->label(FALSE); ?>

        </div>
        <div class="inputs-row clearfix">
            <label class="input-label"><?= $model->getAttributeLabel('password'); ?></label>

            <?= $form->field($model, 'password', [
                'options' => [
                    'class' => 'input-1 input'
                ],
                'template' => '{input}{error}',
            ])->passwordInput()->label(FALSE); ?>

        </div>
    </div>
    <div class="restore"><a href="#" class="replace-modal-form" data-url="<?= Url::toRoute(['/user/security/reset-password']); ?>">Забыли пароль?</a></div>
    <div class="remember">
        <label class="checkbox-label">

            <?= $form->field($model, 'rememberMe', [
                'options' => [
                    'tag' => FALSE
                ],
                'checkboxTemplate' => '{input}'
            ])->checkbox([
                'class' => 'checkbox-1 styled'
            ])->label(FALSE); ?>

            <?= $model->getAttributeLabel('rememberMe'); ?>

        </label>
    </div>
    <div class="buttons">
        <button type="submit" class="submit-btn button-2">Войти</button>
        <span>или</span>
        <a href="#" class="register-btn replace-modal-form" data-url="<?= Url::toRoute(['/raffle/check-ajax/check-user-age', 'form' => TRUE]); ?>">Зарегистрироваться</a>
    </div>

 <?php ActiveForm::end(); ?>