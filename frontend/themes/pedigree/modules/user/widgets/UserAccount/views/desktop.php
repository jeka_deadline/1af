<?php
use yii\helpers\Url;
?>
<div class="userbar">

    <?php if (Yii::$app->user->isGuest) : ?>

        <a href="#" class="modal-open login-btn" data-url="<?= Url::toRoute(['/user/security/login']); ?>">Войти</a>

    <?php else : ?>

        <div class="username"><?= Yii::$app->user->identity->full_name; ?></div>
        <a href="<?= Url::toRoute(['/user/personal-area/index']); ?>" class="cabinet-btn">Личный кабинет</a>
        <a href="<?= Url::toRoute(['/user/security/logout']); ?>" data-method="post" class="logout-btn">&nbsp;</a>

    <?php endif; ?>

</div>