<?php
use yii\helpers\Url;
?>
<div class="userbar">

    <?php if (Yii::$app->user->isGuest) : ?>

        <p><a href="#" class="modal-open" data-url="<?= Url::toRoute(['/user/security/login']); ?>">Войти</a></p>

    <?php else : ?>

        <div class="username"><?= Yii::$app->user->identity->full_name; ?></div>
        <p><a href="<?= Url::toRoute(['/user/personal-area/index']); ?>" class="cabinet-btn">Личный кабинет</a></p>
        <p><a href="<?= Url::toRoute(['/user/security/logout']); ?>" class="logout-link" data-method="post">Выход</a></p>

    <?php endif; ?>

</div>