<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php  $form = ActiveForm::begin([
    'options' => [
        'class' => 'ajax-modal-form content',
    ],
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

    <h2 class="title">Адрес доставки</h2>
    <div class="inputs">
        <div class="inputs-row">
            <div class="heading"><?= $model->getAttributeLabel('address'); ?></div>

                <?= $form->field($model, 'address', [
                    'options' => [
                        'class' => 'input-1'
                    ],
                    'template' => ($model->getErrors('address')) ? '{input}{error}' : '{input}',
                ])->label(FALSE); ?>

        </div>
        <div class="inputs-row captcha">
            <div class="heading"><?= $model->getAttributeLabel('captcha'); ?></div>

            <?= $form->field($model, 'captcha', [
                'options' => [
                    'class' => 'clearfix'
                ],
            ])->widget(\yii\captcha\Captcha::className(), [
                'captchaAction' => '/core/index/captcha',
                'template' => '<div class="image">{image}</div><div class="input-1 input">{input}</div>',
                'options' => [
                    'class' => NULL,
                ],
            ])->label(FALSE); ?>

        </div>
    </div>
    <div class="buttons">
        <button type="submit" class="button-2">Отправить</button>
    </div>

<?php ActiveForm::end(); ?>
