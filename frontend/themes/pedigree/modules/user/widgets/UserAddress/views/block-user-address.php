<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\core\widgets\Kladr\Kladr;
?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'ajax-modal-form delivery'],
    'action' => ['/user/personal-area-ajax/save-address'],
    'errorCssClass' => 'error',
    'fieldConfig' => [
        'errorOptions' => [
            'class' => 'errorMessage'
        ],
    ],
]); ?>

  <h2>Адрес доставки призов</h2>
  <div class="heading">
    Для получения вещественного приза укажите ваш настоящий адрес для доставки. <br>
    Получить приз на Почте Россия можно только по настоящему адресу и ФИО получателя.
  </div>
  <div class="form">
    <div class="form-row">
      <div class="form-col">
          <?= $form->field($model, 'address')->widget(Kladr::className(), [
              'containerOptions' => [
                  'class' => 'input-1',
              ],
              'containerTag' => 'div',
              'defaultOptions' => [
                  'oneString' => TRUE,
              ],
              'options' => [
                  'tag' => FALSE,
              ],
          ])->label(FALSE); ?>

      </div>
      <div class="form-col"><?= Html::a('Не нашли адрес?', '#', ['class' => 'modal-open', 'data' => ['url' => ['/user/personal-area-ajax/modal-not-found-address']]]); ?></div>
    </div>
    <div class="form-row">
      <div class="form-col"><label>Индекс</label><span class="input-1"><input type="text" name="index"></span></div>
      <div class="form-col"><label>Квартира</label><span class="input-1"><input type="text" name="index"></span></div>
    </div>
  </div>
  <button type="submit" class="submit-btn button-2">Сохранить</button>

<?php ActiveForm::end(); ?>