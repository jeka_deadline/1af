<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\core\widgets\Kladr\Kladr;
?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'ajax-modal-form'],
    'action' => ['/user/personal-area-ajax/save-address'],
]); ?>

    <?= $form->field($model, 'address')->widget(Kladr::className(), [
    'options' => [
        'class' => 'form-control',
    ],
    'defaultOptions' => [
        'oneString' => TRUE,
    ],
    ]); ?>

    <?= Html::a('не нашли адрес', '#', ['class' => 'get-modal', 'data' => ['url' => ['/user/personal-area-ajax/modal-not-found-address']]]); ?>

    <?= $form->field($model, 'addressIndex'); ?>

    <?= $form->field($model, 'addressFlat'); ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>