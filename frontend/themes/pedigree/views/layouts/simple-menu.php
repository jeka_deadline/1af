<?php
use yii\widgets\Menu;
use frontend\modules\core\models\Helper;
?>

<?= Menu::widget([
    'items' => [
        [
            'label' => 'Призы', 'url' => ['/core/index/index'],
        ],
        [
            'label' => 'Победители', 'url' => ['/raffle/check/winners'],
        ],
        [
            'label' => 'Вопросы-ответы', 'url' => ['/dictionary/faq/index'],
        ],
        [
            'label' => 'Правила', 'url' => ['/core/index/rules'],
        ],
    ],
]); ?>
