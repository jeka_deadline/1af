<?php
use frontend\modules\core\widgets\TextBlock\TextBlock;
use yii\helpers\Url;
?>
<footer>
    <div class="container">
        <div class="part-top clearfix">
            <div class="copyright">&copy; <?= date('Y'); ?> Mars, Incorporated. Все права защищены.</div>
            <div class="social">
                <span>Поделиться:</span>
                <ul>
                    <li><a href="#" target="_blank" class="vk">&nbsp;</a></li>
                    <li><a href="#" target="_blank" class="fb">&nbsp;</a></li>
                    <li><a href="#" target="_blank" class="ok">&nbsp;</a></li>
                </ul>
            </div>
            <ul class="menu-1">
                <li><a href="#" class='modal-open' data-url="<?= Url::toRoute(['/core/support-ajax/support-message']); ?>">Обратная связь</a></li>
                <li><a href="#">Полные правила акции</a></li>
            </ul>
        </div>
        <ul class="menu-2">
            <li><a href="http://www.mars.com/global/policies/privacy/pp-russian" target="_blank"><img src="<?= $this->theme->getUrl('style/images/icon-confidentiality-1.png'); ?>" alt=""><span>Конфиденциальность</span></a></li>
            <li><a href="http://www.mars.com/global/policies/legal/ld-russian" target="_blank"><img src="<?= $this->theme->getUrl('style/images/icon-legal-1.png'); ?>" alt=""><span>Юридические условия</span></a></li>
            <li><a href="http://www.mars.com/cis/ru/site-owner.aspx" target="_blank"><img src="<?= $this->theme->getUrl('style/images/icon-owner-1.png'); ?>" alt=""><span>Владелец сайта</span></a></li>
        </ul>
        <div class="text">

            <?= TextBlock::widget(['uri' => 'footer-content']); ?>

        </div>
    </div>
</footer>