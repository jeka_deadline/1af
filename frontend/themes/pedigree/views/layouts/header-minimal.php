<?php
use yii\helpers\Url;
?>
<header>
    <div class="container clearfix">
        <a href="/" class="logo"><img src="<?= $this->theme->getUrl('style/images/pedigree-logo-1.png'); ?>" alt=""></a>
        <div class="part-top clearfix"></div>
        <div class="part-bottom clearfix">
            <a href="#" class="menu-btn">&nbsp;</a>
            <a href="#" class="button-1 right-btn modal-open" data-url="<?= Url::toRoute(['/raffle/check-ajax/check-user-age']); ?>">Участвовать в акции</a>

            <?= $this->render('simple-menu'); ?>

        </div>
    </div>
</header>