<?php
use yii\widgets\Menu;
use frontend\modules\user\widgets\UserAccount\UserAccount;
use yii\helpers\Url;
?>
<div class="mobile-menu">
    <a href="#" class="menu-btn">&nbsp;</a>

    <?= UserAccount::widget([
        'template' => 'mobile',
    ]); ?>

    <?= $this->render('simple-menu'); ?>
</div>