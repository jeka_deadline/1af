<?php
use yii\helpers\Html;
use frontend\assets\PedigreeAsset;
use frontend\modules\raffle\assets\CheckAsset;

PedigreeAsset::register($this);
CheckAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
  </head>
  <?php $this->beginBody() ?>
  <body>

      <?= $content; ?>

      <?= ''//$this->render('modal'); ?>

      <div id="modal_modals" class="modal modal-1"></div>

      <?php $this->endBody() ?>

  </body>
</html>
<?php $this->endPage() ?>