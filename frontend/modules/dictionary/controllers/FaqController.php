<?php
namespace frontend\modules\dictionary\controllers;

use Yii;
use frontend\modules\dictionary\models\Termin;
use frontend\modules\core\components\FrontController;

class FaqController extends FrontController
{

    public function actionIndex()
    {
        return $this->render('faq', [
            'contentFaq' => Termin::generateListFaq(),
        ]);
    }

}