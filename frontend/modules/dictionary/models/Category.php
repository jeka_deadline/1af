<?php
namespace frontend\modules\dictionary\models;

use Yii;
use common\models\dictionary\Category as BaseCategory;

class Category extends BaseCategory
{

    public function getItemsList()
    {
        return $this->hasMany(Termin::className(), ['category_id' => 'id']);
    }

}