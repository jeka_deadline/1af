<?php
namespace frontend\modules\dictionary\models;

use Yii;
use common\models\dictionary\Termin as BaseTermin;
use yii\helpers\ArrayHelper;

class Termin extends BaseTermin
{

    public static function generateListFaq()
    {
        $list = [];

        $categories = Category::find()
                                    ->where(['active' => 1])
                                    ->orderBy(['display_order' => SORT_ASC])
                                    ->all();

        foreach ($categories as $index => $category) {
            $list[ $index ] = [
                'category-name' => $category->name,
                'items'         => $category
                                        ->getItemsList()
                                        ->where(['active' => 1])
                                        ->orderBy(['display_order' => SORT_ASC])
                                        ->all(),
            ];
        }

        return $list;
    }

}