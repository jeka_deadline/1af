<?php
namespace frontend\modules\dictionary;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'frontend\modules\dictionary\controllers';

    public function init()
    {
        return parent::init();
    }

}