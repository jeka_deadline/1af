<?php
namespace frontend\modules\raffle;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'frontend\modules\raffle\controllers';

    public function init()
    {
        return parent::init();
    }

}