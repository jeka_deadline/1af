<?php
namespace frontend\modules\raffle\models;

use common\models\raffle\Store as BaseStore;

class Store extends BaseStore
{

    public function getFullImagePath()
    {
        return DIRECTORY_SEPARATOR . static::getFilePath() . DIRECTORY_SEPARATOR . $this->image;
    }

}