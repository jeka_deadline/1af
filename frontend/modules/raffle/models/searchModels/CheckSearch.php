<?php

namespace frontend\modules\raffle\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\raffle\models\Check;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\User;

/**
 * CheckSearch represents the model behind the search form about `frontend\modules\raffle\models\Check`.
 */
class CheckSearch extends Check
{
    public $userNameOrPhone;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_id', 'user_id', 'status', 'prize_id'], 'integer'],
            [['number', 'check_date', 'check_image', 'register_date', 'reason_rejection', 'winner_date', 'userNameOrPhone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Check::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_id' => $this->store_id,
            'user_id' => $this->user_id,
            'check_date' => $this->check_date,
            'register_date' => $this->register_date,
            'status' => $this->status,
            'prize_id' => $this->prize_id,
            'winner_date' => $this->winner_date,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'check_image', $this->check_image])
            ->andFilterWhere(['like', 'reason_rejection', $this->reason_rejection]);

        $query->andWhere(['not', ['prize_id' => NULL]]);
        $query->andWhere(['status' => self::STATUS_ACCEPT]);
        $query->with('user', 'prize');

        if ($this->userNameOrPhone) {
            $userTableName = User::tableName();
            $query->joinWith('user', TRUE, 'INNER JOIN')->andWhere(['or', ['like', $userTableName . '.full_name', $this->userNameOrPhone], ['like', $userTableName . '.phone', $this->userNameOrPhone]]);
        }

        $query->orderBy(['winner_date' => SORT_DESC]);

        return $dataProvider;
    }
}
