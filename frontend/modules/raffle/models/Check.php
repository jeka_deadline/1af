<?php
namespace frontend\modules\raffle\models;

use common\models\raffle\Check as BaseCheck;
use frontend\modules\user\models\User;
use yii\helpers\Url;
use yii\helpers\Html;

class Check extends BaseCheck
{

    public function getPrizeStatus()
    {
        $user = $this->user;

        switch ($this->status) {
            case self::STATUS_REJECT:
                return 'Отклонен';
            case self::STATUS_ACCEPT:
                return ($user->address) ? 'Готово' : Html::a('Заполните адрес', ['/user/personal-area/index', 'tab' => 'delivery']);
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getPrize()
    {
        return $this->hasOne(Prize::className(), ['id' => 'prize_id']);
    }

    public function getLinkSend()
    {
        return ($this->send_number) ? Html::a('https://www.pochta.ru/tracking#' . $this->send_number, 'https://www.pochta.ru/tracking#' . $this->send_number, ['target' => '_blank']) : NULL;
    }

    public function getSendStatus()
    {
        return $this->hasOne(SendStatus::className(), ['id' => 'send_status_id']);
    }

}