<?php
namespace frontend\modules\raffle\controllers;

use Yii;
use frontend\modules\core\components\FrontController;
use frontend\modules\raffle\models\Check;
use frontend\modules\raffle\models\searchModels\CheckSearch;
use yii\grid\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use frontend\modules\raffle\models\Prize;

class CheckController extends FrontController
{

    public function actionWinners()
    {
        $searchModel = new CheckSearch();
        $searchModel->load(Yii::$app->request->get());

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('winners', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listPrizes' => ArrayHelper::map(Prize::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'name'),
            'listDates' => ArrayHelper::map(Check::find()->where(['not', ['prize_id' => NULL]])->orderBy(['winner_date' => SORT_DESC])->all(), 'winner_date', function($model){ return Yii::$app->formatter->asDate($model->winner_date, 'php:Y/m/d');}),
        ]);
    }

}