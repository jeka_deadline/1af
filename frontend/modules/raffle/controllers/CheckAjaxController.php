<?php
namespace frontend\modules\raffle\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\user\models\forms\CheckUserAgeForm;
use yii\web\Response;
use frontend\modules\user\models\forms\RegisterCheckStep1Form;
use frontend\modules\user\models\forms\RegisterCheckStep2Form;
use yii\widgets\ActiveForm;

class CheckAjaxController extends Controller
{

    public function actionCheckUserAge()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->request->cookies;
        $response = [
            'html' => NULL,
            'reload' => NULL,
        ];

        if (!self::checkUserAge()) {
            return [
                'reload' => TRUE,
            ];
        }

        $model = new CheckUserAgeForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->checkUserAge()) {

                $response[ 'reload' ] = TRUE;
            } else {
                $response[ 'html' ] = $this->renderAjax('check-user-age', [
                    'model' => $model,
                ]);
            }

            return $response;
        }

        $response[ 'html' ] = $this->renderAjax('check-user-age', [
            'model' => $model,
        ]);

        return $response;
    }

    public function actionRegisterCheckStep1()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cookies = Yii::$app->request->cookies;
        $response = [
            'html' => NULL,
            'modal' => NULL,
            'callback'=>NULL,
        ];

        if (self::checkUserAge()) {
            $model = new CheckUserAgeForm();

            return [
                'html' => $this->renderAjax('check-user-age', [
                                'model' => $model,
                            ]),
            ];
        }

        if (!Yii::$app->user->isGuest) {
            $model              = new RegisterCheckStep2Form();
            $response[ 'modal' ] = $response[ 'html' ] = $this->renderAjax('register-check-step2', ['model' => $model]);

            return $response;
        }

        $model = new RegisterCheckStep1Form();

        if ($model->load(Yii::$app->request->post())) {
            $tmpPhone=$model->phone;
            if ($model->next()) {
                $model = new RegisterCheckStep2Form();
                $response['callback'] = 'callbackPanelUserAuthorized';
                $response[ 'modal' ] = $this->renderAjax('register-check-step2', [
                    'model' => $model,
                ]);
            } else {
                $model->scenario=RegisterCheckStep1Form::SCENARIO_NOT_FILTER;
                $model->phone=$tmpPhone;
                $response[ 'html' ] = $this->renderAjax('register-check-step1-form', ['model' => $model]);
            }
        } else {
            $model = $this->getNewFormRegisterCheckStep1();

            $response[ 'html' ]   = $this->renderAjax('register-check-step1', ['model' => $model]);
        }

        return $response;
    }

    public function actionSendSms()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cookies = Yii::$app->request->cookies;
        $response = [
            'html' => NULL,
            'code' => NULL,
        ];

        if (self::checkUserAge()) {
            $model = new CheckUserAgeForm();

            return [
                'html' => $this->renderAjax('check-user-age', [
                                'model' => $model,
                            ]),
            ];
        }

        $model = new RegisterCheckStep1Form();

        $model->load(Yii::$app->request->post());

        $code = RegisterCheckStep1Form::generateSmsCode();

        $tpl = $this->renderAjax('@frontend/modules/raffle/views/sms/phone', [
            'code'      => $code,
            'login'     => Yii::$app->params['sms']['login'],
            'password'  => Yii::$app->params['sms']['password'],
            'sendPhone' => $model->phone,
        ]);

        $tmpPhone=$model->phone;
        if ($model->sendSms($code, $tpl)) {
            $model->smsCode           = NULL;
            $model->name              = NULL;
            $model->permanentPassword = NULL;
        } else {
            if ($model->getErrors('phone')) {
                $model->isRegister = 1;
            }
        }

        $model->scenario=RegisterCheckStep1Form::SCENARIO_NOT_FILTER;
        $model->phone=$tmpPhone;

        $response[ 'html' ] = $this->renderAjax('register-check-step1-form', ['model' => $model]);

        return $response;
    }

    public function actionRegisterCheckStep2($staticPage = FALSE)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cookies = Yii::$app->request->cookies;
        $response = [
            'html' => NULL,
            'modal' => NULL,
            'form' => NULL,
        ];

        if (!$staticPage) {

            if (self::checkUserAge()) {
                $model = new CheckUserAgeForm();

                return [
                    'html' => $this->renderAjax('check-user-age', [
                                    'model' => $model,
                                ]),
                ];
            }

            if (Yii::$app->user->isGuest) {
                $model = $this->getNewFormRegisterCheckStep1();

                $response[ 'html' ] = $this->renderAjax('register-check-step1', ['model' => $model]);
            }

        }

        $model = new RegisterCheckStep2Form();

        if ($staticPage) {
            $model->storeId = 1;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->registerCheck()) {
                if ($staticPage) {
                    $response[ 'modal' ]      = $this->renderAjax('success-register-check-modal');
                    $response[ 'showModal' ]  = TRUE;
                    $response[ 'reload' ]     = TRUE;
                } else {
                    $response[ 'html' ] = $this->renderAjax('success-register-check');
                }
            } else {
                if ($staticPage) {
                    $response[ 'html' ] = $this->renderAjax('form-register-check-page-rules', ['model' => $model]);
                } else {
                    $response[ 'html' ] = $this->renderAjax('register-check-step2-form', ['model' => $model]);
                }
            }

            return $response;
        }

        $response[ 'html' ] = $this->renderAjax('register-check-step2', ['model' => $model]);

        return $response;
    }

    private function getNewFormRegisterCheckStep1()
    {
        $model              = new RegisterCheckStep1Form();
        $model->scenario    = RegisterCheckStep1Form::SCENARIO_LOGIN;
        $model->isRegister  = 1;

        return $model;
    }

    public function beforeAction($action)
    {
        Yii::$app->assetManager->bundles = [
            'yii\bootstrap\BootstrapPluginAsset' => false,
            'yii\bootstrap\BootstrapAsset' => false,
            'yii\web\JqueryAsset' => false,
        ];

        return TRUE;
    }

    public static function checkUserAge()
    {
        $cookies = Yii::$app->request->cookies;

        if (!$cookies->has('check-user-age')) {
            return TRUE;
        }

        return FALSE;
    }

    public function actionGetCheckExampleImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->request->cookies;
        $response = [
            'html' => NULL,
        ];


        $response[ 'html' ] = $this->renderPartial('modal-check-example-image');

        return $response;
    }

}