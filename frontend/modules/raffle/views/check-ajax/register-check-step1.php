<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'id' => 'modal-form-default',
    'header' => 'Регистрация чека',
]); ?>

    <h2>Регистрация чека, шаг 1 из 2</h1>
    <h4>Заполните пожалуйста поля ниже</h4>

    <?= $this->render('register-check-step1-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>