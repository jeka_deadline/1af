<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/raffle/check-ajax/register-check-step1'],
    'options' => [
        'class' => 'ajax-modal-form',
        'data' => ['url' => ['/raffle/check-ajax/send-sms']]
    ],
]); ?>

    <?= $form->field($model, 'phone'); ?>

    <?= $form->field($model, 'isRegister')->checkbox(['class' => 'js-is-register']); ?>

    <?= $form->field($model, 'password', ['options' => ['class' => ($model->isRegister) ? 'form-group' : 'form-group hide']])->passwordInput(['id' => 'input-login-password']); ?>

    <div style="color:#999;margin:1em 0">
        <?= Html::a('забыли пароль?', ['/user/security/reset-password'], ['id' => 'btn-reset-password', 'class' => ($model->isRegister) ? 'form-group' : 'form-group hide']); ?>
    </div>

    <?= Html::beginTag('div', ['class' => ($model->isRegister || $model->getErrors('phone')) ? 'hide' : '', 'id' => 'block-sms']); ?>

        <?= ''//$form->field($model, 'isSmsSend')->checkbox(['class' => 'js-send-sms-code', 'data' => ['url' => ['/raffle/check-ajax/send-sms']]]); ?>

        <?= $form->field($model, 'smsCode'); ?>

        <?= Html::a('не пришел код', '#', ['id' => 'resend-sms']) ?>

    <?= Html::endTag('div'); ?>

    <?= Html::beginTag('div', ['class' => ($model->isValidCode() ? '' : 'hide'), 'id' => 'block-register']); ?>

        <?= $form->field($model, 'name'); ?>

        <?= $form->field($model, 'permanentPassword')->passwordInput(); ?>

    <?= Html::endTag('div'); ?>

    <?= Html::submitButton('Далее', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>