<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use trntv\yii\datetime\assets\DateTimeAsset;
use trntv\yii\datetime\assets\MomentAsset;

//DateTimeAsset::register($this);
//MomentAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => ['/raffle/check-ajax/register-check-step2'],
    'options' => [
        'class' => 'ajax-modal-form',
        'enctype'=>'multipart/form-data',
    ],
]); ?>

    <?= $form->field($model, 'storeId')->radioList($model->getStores()); ?>

    <?= $form->field($model, 'numberCheck'); ?>

    <?= $form->field($model, 'dateCheck')->textInput(['placeholder' => date('d-m-Y')]); ?>

    <?= $form->field($model, 'timeCheck')->textInput(['placeholder' => date('H:i')]); ?>

    <?= $form->field($model, 'imageCheck')->fileInput(['accept' => '.png, .jpeg, .jpg, .bmp, .pdf']); ?>

    <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
        'captchaAction' => '/core/index/captcha',
    ]) ?>

    <?= $form->field($model, 'agreeRule')->checkbox(); ?>

    <?= Html::submitButton('Далее', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>