<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin(['id' => 'modal-form-default']); ?>

    <?= $this->render('check-user-age-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>