<?= '<?xml version="1.0" encoding="utf-8" ?>' ?>
<request>
    <message type="sms">
        <sender>Yota</sender>
        <text>Код: <?= $code; ?></text>
        <phone cell="<?= $sendPhone; ?>" work="<?= $sendPhone; ?>" fax="<?= $sendPhone; ?>"/>
        <abonent phone="<?= $sendPhone; ?>" number_sms="1" />
    </message>
    <security>
        <login value="<?= $login; ?>" />
        <password value="<?= $password; ?>" />
    </security>
</request>