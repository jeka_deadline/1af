<?php
namespace frontend\modules\raffle\assets;

use yii\web\AssetBundle;

class CheckAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/pedigree/';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->publishOptions[ 'forceCopy' ] = TRUE;
        $this->js[] = 'js/check.js';
    }
}
