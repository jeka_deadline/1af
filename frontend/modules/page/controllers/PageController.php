<?php
namespace frontend\modules\page\controllers;

use frontend\modules\core\components\FrontController;
use frontend\modules\page\models\Page;
use yii\web\NotFoundHttpException;
use common\models\core\Helper as CommonHelper;

class PageController extends FrontController
{

    public function actionIndex($uri)
    {
        $page = Page::find()
                          ->where(['uri' => $uri, 'active' => 1])
                          ->one();

        if (!$page) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        $pageMeta = CommonHelper::getPageMetaForModel($page->id, get_class($page));

        if ($pageMeta) {
            $this->setMeta($pageMeta);
        }

        return $this->render('index', [
            'page' => $page,
        ]);
    }

}