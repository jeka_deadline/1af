<?php
namespace frontend\modules\page;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'frontend\modules\page\controllers';

    public function init()
    {
        return parent::init();
    }

}