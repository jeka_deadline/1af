<?php
namespace frontend\modules\core\widgets\TextBlock;

use Yii;
use yii\base\Widget;
use frontend\modules\core\models\TextBlock as ModelTextBlock;

class TextBlock extends Widget
{
    public $uri;

    public function run()
    {
        $model = ModelTextBlock::find()
                                    ->where(['active' => 1, 'uri' => $this->uri])
                                    ->one();

        if ($model) {
            return $model->content;
        }

        return NULL;
    }

}