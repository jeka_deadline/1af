<?php
namespace frontend\modules\core\widgets\Kladr;

use Yii;
use andkon\yii2kladr\Kladr as BaseKladr;
use yii\helpers\Json;
use andkon\yii2kladr\assets\KladrAsset;
use yii\helpers\Html;

class Kladr extends BaseKladr
{

    public $defaultOptions = [];

    public function init()
    {
        if (!$this->name) {
            $this->name  = Html::getInputName($this->model, $this->attribute);
            $this->id    = Html::getInputId($this->model, $this->attribute);
            $this->value = $this->model->{$this->attribute};
        }

        KladrAsset::register($this->getView());
    }

    protected function registryJsForInput($fakeId, $id)
    {
        switch ($this->type) {
            case self::TYPE_STREET:
                $script = '$("#' . $this->containerId . ' #' . $fakeId . '")
                .kladr({type: "' . $this->type . '", parentType: $.kladr.type.city,
                parentInput:"#' . self::$inputs[self::TYPE_CITY][1] . '"})';
                break;
            case self::TYPE_BUILDING:
                $script = '$("#' . $this->containerId . ' #' . $fakeId . '")
                .kladr({type: "' . $this->type . '", parentType: $.kladr.type.street,
                parentInput:"#' . self::$inputs[self::TYPE_STREET][1] . '"})';
                break;
            case self::TYPE_ZIP:
                $zipJs = '$("#' . self::$inputs[self::TYPE_BUILDING][1] . '")
                .kladr("select", function(obj){
                    if(obj.zip){
                        $("#' . self::$inputs[self::TYPE_ZIP][0] . '").val(obj.zip);
                        $("#' . self::$inputs[self::TYPE_ZIP][1] . '").val(obj.zip);
                    }
                });';
                $this->getView()->registerJs($zipJs);

                $script = '$("#' . $this->containerId . ' #' . $fakeId . '")';
                break;
            default:
                $options = Json::encode($this->defaultOptions);
                $script = '$("#' . $this->containerId . ' #' . $fakeId . '").kladr(' . $options . ')';
        }

        $script .= '.change(
            function(event){
                $("#' . $this->containerId . ' #' . $id . '").val($(event.target).attr("data-kladr-id"));
            }
        )';

        $this->getView()->registerJs($script);
    }

}