<?php
namespace frontend\modules\core\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\core\models\forms\SupportForm;
use yii\web\Response;

class SupportAjaxController extends Controller
{

    public function actionSupportMessage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'html' => NULL,
        ];
        $model = new SupportForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->createSupportMessage()) {
                $response[ 'html' ] = $this->renderPartial('success-create-support-message');
            } else {
                $response[ 'html' ] = $this->renderPartial('support-form', [
                    'model' => $model,
                ]);
            }
        } else {
            $response[ 'html' ] = $this->renderPartial('support-modal', [
                'model' => $model,
            ]);
        }

        return $response;
    }

}