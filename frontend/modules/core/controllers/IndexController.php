<?php
namespace frontend\modules\core\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\modules\core\components\FrontController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\core\models\ContactForm;
use frontend\modules\user\models\forms\RegisterCheckStep1Form;
use frontend\modules\core\models\Page;
use frontend\modules\core\components\CaptchaAction;
use yii\web\Response;
use frontend\modules\user\models\forms\RegisterCheckStep2Form;

class IndexController extends FrontController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => CaptchaAction::className(),
                'offset' => 10,
                'width' => 164,
                'height' => 40,
                'foreColor' => '0x000',
                //'fontSize' => 30,
                'fontFile' => '@app/modules/core/fonts/FuturaOrtoLt/a_FuturaOrtoLt_LightItalic.TTF',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCheckUserAge()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->request->cookies;

        $response = [
            'html' => NULL,
        ];

        /*if (!Yii::$app->user->isGuest) {
            $model              = new RegisterCheckStep2Form();
            $response[ 'html' ] = $response[ 'html' ] = $this->renderAjax('register-check-step2', ['model' => $model]);

            return $response;
        }*/

        if (!$cookies->has('check-user-age')) {

            $model = $this->getNewFormRegisterCheckStep1();

            $response[ 'html' ]   = $this->renderAjax('register-check-step1', ['model' => $model]);

            return $response;
        }
    }

    public function actionRules()
    {
        $model = new RegisterCheckStep2Form();
        $model->storeId = 1;

        return $this->render('rules', [
            'model' => $model,
        ]);
    }

}