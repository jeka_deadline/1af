<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'id' => 'modal-form-default',
    'header' => 'Задать вопрос',
]); ?>

    <?= $this->render('support-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>