<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'ajax-modal-form'],
]); ?>

    <?= $form->field($model, 'name'); ?>

    <?= $form->field($model, 'email'); ?>

    <?= $form->field($model, 'subjectId')->dropDownList($model->getListSupportSubjects(), ['prompt' => 'Выберите тему сообщения']); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 10]); ?>

    <?= $form->field($model, 'image')->fileInput(); ?>

    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>