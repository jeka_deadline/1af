<?php
namespace frontend\modules\core\components;

use Yii;
use yii\web\Controller;
use frontend\moduls\core\models\PageMeta;
use frontend\modules\core\models\Page;
use common\models\core\Helper as CommonHelper;

class FrontController extends Controller
{

    public function setMetaTitle($title = '')
    {
        $this->view->title = $title;
    }

    public function setMetaDescription($description = '')
    {
        Yii::$app->view->registerMetaTag([
            'name'    => 'description',
            'content' => $description,
        ]);
    }

    public function setMetaKeywords($keywords = '')
    {
        Yii::$app->view->registerMetaTag([
            'name'    => 'keywords',
            'content' => $keywords,
        ]);
    }

    public function setMeta($pageMeta)
    {
        $this->setMetaTitle($pageMeta->meta_title);
        $this->setMetaDescription($pageMeta->meta_description);
        $this->setMetaKeywords($pageMeta->meta_keywords);
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return FALSE;
        }

        $page = Page::find()
                        ->where(['route' => $this->getRoute(), 'active' => 1])
                        ->one();

        if (!$page) {
            return TRUE;
        }

        $meta = CommonHelper::getPageMetaForModel($page->id, get_class($page));

        if ($meta) {
            $this->setMeta($meta);
        }

        return TRUE;
    }

}