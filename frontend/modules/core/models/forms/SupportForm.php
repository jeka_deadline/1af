<?php
namespace frontend\modules\core\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\core\models\SupportSubject;
use frontend\modules\core\models\SupportMessage;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class SupportForm extends Model
{

    public $name;
    public $email;
    public $subjectId;
    public $text;
    public $image;

    public function rules()
    {
        return [
            [['name', 'email', 'subjectId', 'text'], 'required'],
            [['name', 'email'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['text'], 'string'],
            [['subjectId'], 'exist', 'targetClass' => SupportSubject::className(), 'targetAttribute' => 'id', 'filter' => function($query){
                  return $query->andWhere(['active' => 1]);
            }],
            [['image'], 'file', 'extensions' => 'bmp, jpeg, jpg, png', 'skipOnEmpty' => TRUE],
        ];
    }

    public function getListSupportSubjects()
    {
        return ArrayHelper::map(SupportSubject::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'name');
    }

    public function createSupportMessage()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $model = new SupportMessage();
        $model->subject_id = $this->subjectId;
        $model->name = $this->name;
        $model->email = $this->email;
        $model->text = $this->text;

        $file = UploadedFile::getInstance($this, 'image');

        if ($file) {
            $fileName = md5($file->baseName);
            $uploadPath = Yii::getAlias('@frontend/web') . DIRECTORY_SEPARATOR . SupportMessage::getFilePath();

            if (!is_file($uploadPath) || !is_dir($uploadPath)) {
                mkdir($uploadPath, 0777);
            }

            $file->saveAs($uploadPath. DIRECTORY_SEPARATOR . $fileName . '.' . $file->extension);
            $model->image = $fileName . '.' . $file->extension;
        }

        if ($model->validate() && $model->save()) {
            return TRUE;
        }

        return FALSE;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'email' => 'Ваш e-mail',
            'subjectId' => 'Тема обращения',
            'text' => 'Сообщение',
            'image' => 'Изображение чека',
        ];
    }

}