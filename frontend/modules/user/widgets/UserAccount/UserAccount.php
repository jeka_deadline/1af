<?php
namespace frontend\modules\user\widgets\UserAccount;

use Yii;
use yii\base\Widget;
use frontend\modules\user\models\User;

class UserAccount extends Widget
{

    public $template;

    public function run()
    {
        return $this->render($this->template);
    }
}
?>