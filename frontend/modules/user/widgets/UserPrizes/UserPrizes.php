<?php
namespace frontend\modules\user\widgets\UserPrizes;

use Yii;
use yii\base\Widget;
use frontend\modules\user\models\User;
use frontend\modules\raffle\models\Check;

class UserPrizes extends Widget
{

    public function run()
    {

        $user = User::findOne(Yii::$app->user->identity->getId());

        if (!$user) {
            return FALSE;
        }

        $checks = Check::find()
                            ->where(['not', ['prize_id' => NULL]])
                            ->andWhere(['user_id' => $user->id])
                            ->andWhere(['<>', 'status', Check::STATUS_PROCESSING])
                            ->all();

        return $this->render('user-winner-checks', [
            'checks' => $checks,
        ]);

    }

}
?>