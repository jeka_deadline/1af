<table>
  <thead>
    <tr>
      <th>Название приза</th>
      <th>Дата выигриша</th>
      <th>Номер чека</th>
      <th>Статус</th>
    </tr>
  </thead>
  <tbody>

    <?php foreach ($checks as $check) : ?>

        <tr>
            <td><?= $check->prize->name; ?></td>
            <td><?= $check->winner_date; ?></td>
            <td><?= $check->number; ?></td>
            <td><?= $check->getPrizeStatus(); ?></td>
        </tr>

    <?php endforeach; ?>

  </tbody>
</table>