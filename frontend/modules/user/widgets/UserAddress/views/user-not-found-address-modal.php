<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'id' => 'modal-form-default',
    'header' => 'Сообщение',
]); ?>

    <p>Введите адрес, который не удалось найти</p>

    <?= $this->render('user-not-found-address-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>