<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php  $form = ActiveForm::begin([
    'options' => [
        'class' => 'ajax-modal-form',
    ]
]); ?>

    <?= $form->field($model, 'address'); ?>

    <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
        'captchaAction' => '/core/index/captcha',
    ]) ?>

    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>