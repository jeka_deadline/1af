<?php
namespace frontend\modules\user\models;

use Yii;
use common\models\user\User as BaseUser;
use frontend\modules\raffle\models\Check;

class User extends BaseUser
{

    public function getHiddenPhone()
    {
        return substr($this->phone, 0, 1) . ' (' . substr($this->phone, 1, 3) . ')*****' . substr($this->phone, -2);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public static function findByEmailOrPhone($email)
    {
        return static::find()
                          ->where(['or', ['email' => $email], ['phone' => $email]])
                          ->one();
    }

    public static function findByPhone($phone)
    {
        return static::find()
                            ->where(['phone' => $phone])
                            ->one();
    }

    public function isUserWinner()
    {
        return Check::find()
                        ->where(['user_id' => $this->id])
                        ->andWhere(['not', ['prize_id' => NULL]])
                        ->count();
    }
}