<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;

class CheckUserAgeForm extends Model
{

    public $dayBirth;
    public $monthBirth;
    public $yearBirth;

    public function rules()
    {
        return [
            [['dayBirth', 'monthBirth', 'yearBirth'], 'required'],
            [['dayBirth'], 'number'],
            [['monthBirth'], 'number', 'min' => 1, 'max' => 12],
            [['yearBirth'], 'number', 'max' => date('Y')],
            ['dayBirth', 'validateDate'],
        ];
    }

    public function validateDate()
    {
        if ($this->dayBirth && $this->monthBirth && $this->yearBirth) {
            if (!checkdate($this->monthBirth, $this->dayBirth, $this->yearBirth)) {
                $this->addError('dayBirth', 'Неправильная дата');
            }
        }
    }

    public function getListMonths()
    {
        return [
          '1' => 'Январь',
          '2' => 'Февраль',
          '3' => 'Март',
          '4' => 'Апрель',
          '5' => 'Май',
          '6' => 'Июнь',
          '7' => 'Июль',
          '8' => 'Август',
          '9' => 'Сентябрь',
          '10' => 'Октябрь',
          '11' => 'Ноябрь',
          '12' => 'Декабрь',
        ];
    }

    public function checkUserAge()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $cookies = Yii::$app->response->cookies;

        $age = $this->getAge($this->yearBirth, $this->monthBirth, $this->dayBirth);

        if ($age > 18) {
            $cookies->add(new \yii\web\Cookie([
                'name' => 'check-user-age',
                'value' => 'yes',
                'expire' => time() + 6555555,
            ]));
            return TRUE;
        }

        $this->addError('yearBirth', 'Ваш возраст меньше 18 лет');
        return FALSE;
    }

    private function getAge($y, $m, $d)
    {
        if ($m > date('m') || $m == date('m') && $d > date('d')) {
          return (date('Y') - $y - 1);
        } else {
          return (date('Y') - $y);
        }
    }

    public function getListYears()
    {
        $years = range(1900, date('Y'));
        return array_combine($years, $years);
    }

    public function getListDays()
    {
        $days = range(1, 31);
        return array_combine($days, $days);
    }

}