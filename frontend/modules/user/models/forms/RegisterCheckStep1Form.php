<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class RegisterCheckStep1Form extends Model
{

    public $phone;
    public $isRegister;
    public $password;
    public $isSmsSend;
    public $smsCode;
    public $name;
    public $permanentPassword;

    private $_isValidCode = FALSE;
    private $_isFirstBlockName = TRUE;

    const SCENARIO_LOGIN = 'login';
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_SMS_CODE = 'sms-code';
    const SCENARIO_NOT_FILTER = 'not-filter';

    public function rules()
    {
        return [
            [['phone'], 'required'],
            //[['phone'], 'match', 'pattern' => '/^\+?\d+$/'],
            [['phone'], 'filter', 'filter' => function($value) {
                return preg_replace('/\D/', '', $value);
            }],
            [['phone'], 'filter', 'filter' => function($value) {
                return $value;
            }, 'on' => self::SCENARIO_NOT_FILTER],
            [['isRegister', 'isSmsSend'], 'boolean'],
            [['password', 'smsCode', 'name', 'permanentPassword'], 'safe'],
            [['password'], 'required', 'on' => self::SCENARIO_LOGIN],
            [['name', 'permanentPassword', 'smsCode'], 'required', 'on' => self::SCENARIO_REGISTER],
            [['smsCode'], 'required', 'on' => self::SCENARIO_SMS_CODE],
        ];
    }

    public function next()
    {
        $this->userSetScenarion();
        if (!$this->validate()) {
            $this->checkFirstShowBlockName();
            return FALSE;
        }

        if ($this->isRegister) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError('phone', 'Не правильный логин или пароль');
                return FALSE;
            }

            if (!Yii::$app->security->validatePassword($this->password, $user->password_hash)) {
                $this->addError('phone', 'Не правильный логин или пароль');
                return FALSE;
            }

            Yii::$app->user->login($user);

            return TRUE;
        } else {
            if ($this->smsCode) {
                $this->isSmsSend = TRUE;
                $user = $this->getUserByCode();

                if (!$user) {
                    $this->addError('smsCode', 'Неправильный код');
                    return FALSE;
                }

                $this->_isValidCode = TRUE;

                if ($this->name && $this->permanentPassword) {
                    $user->full_name        = $this->name;
                    $user->confirm_phone_at = new Expression('NOW()');
                    $user->confirm_code     = NULL;

                    $user->setPassword($this->permanentPassword);

                    $user->save(FALSE);

                    Yii::$app->user->login($user);

                    return TRUE;
                }
            }
        }
    }

    public function userSetScenarion()
    {
        if ($this->isRegister) {
            $this->scenario = self::SCENARIO_LOGIN;
            return;
        }

        if ($this->smsCode) {
            $user = $this->getUserByCode();
            if ($user) {
                $this->_isValidCode = TRUE;
                $this->scenario = self::SCENARIO_REGISTER;
                return;
            }
        }

        $this->scenario = self::SCENARIO_SMS_CODE;
    }

    public function sendSms($code, $smsTpl)
    {

        $this->checkFirstShowBlockName();
        if (!$this->validate()) {

            return FALSE;
        }

        $user = User::find()
                          ->where(['phone' => $this->phone])
                          ->one();

        if ($user) {
            if ($user->confirm_phone_at) {
                $this->addError('phone', 'Пользователь с данным телефоном уже зарегистрирован');

                return FALSE;
            }
        } else {
            $user = new User();
            $user->auth_key     = Yii::$app->security->generateRandomString(32);
            $user->register_ip  = Yii::$app->request->userIp;
            $user->phone        = $this->phone;
        }

        $user->confirm_code = $code;

        if ($user->validate() && $user->save()) {
            $result = self::sendSmsGateway($smsTpl, Yii::$app->params[ 'sms' ][ 'host' ]);

            if ($result && $result->information == 'send') {
                $this->isSmsSend = TRUE;
                return TRUE;
            }
        }

        return FALSE;
    }

    private function getUserByCode()
    {
        $phone = preg_replace('/\D/', '', $this->phone);

        return User::find()
                          ->where(['phone' => $phone, 'confirm_code' => $this->smsCode])
                          ->andWhere(['IS', 'confirm_phone_at', NULL])
                          ->one();
    }

    private function getUser()
    {
        return User::find()
                        ->where(['phone' => $this->phone])
                        ->andWhere(['not', ['confirm_phone_at' => NULL]])
                        ->andWhere(['IS', 'blocked_at', NULL])
                        ->one();
    }

    private function checkSmsCode()
    {
        $user = $this->getUser();

        if (!$user || $user->confirm_code !== $this->smsCode || $user->confirm_phone_at) {
            return FALSE;
        }

        return TRUE;
    }

    public function isValidCode()
    {
        return $this->_isValidCode;
    }

    public static function generateSmsCode()
    {
        while (TRUE) {
            $code = '';
            for ($i = 0; $i < 5; $i++) {
                $code .= mt_rand(0, 9);
            }

            $uniqueCode = User::find()
                                    ->where(['confirm_code' => $code])
                                    ->one();

            if (!$uniqueCode) {
                return $code;
            }
        }
    }

    public static function sendSmsGateway($body, $href)
    {
        if (!empty($href)) {
            $respnse  ='';
            $ch       = curl_init();

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml; charset=utf-8'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CRLF, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_URL, $href);

            $result = curl_exec($ch);

            curl_close($ch);

            return simplexml_load_string($result);
        }
        return FALSE;
    }

    public function attributeLabels()
    {
        return [
            'phone'             => 'Номер телефона',
            'password'          => 'Введите пароль',
            'isRegister'        => 'Я уже зарегистрирован(-а)',
            'isSmsSend'         => 'Отправлен код проверки по СМС',
            'smsCode'           => 'Код из СМС',
            'name'              => 'Ваше имя',
            'permanentPassword' => 'Введите постоянный пароль',
        ];
    }

    private function checkFirstShowBlockName()
    {
        $session = Yii::$app->session;
        if ($this->scenario === self::SCENARIO_REGISTER) {

            if (!$session->has('firstShowBlockName')) {
                $this->clearErrors();
                $session->set('firstShowBlockName', 'yes');
            }
        } else {
            if ($session->has('firstShowBlockName')) {
                $session->remove('firstShowBlockName');
            }
        }
    }

    public function getAttributeLabelForStatusSms()
    {
        if ($this->isSmsSend) {
            return 'Отправлен код проверки по СМС';
        }

        return 'Подтвердить телефон';
    }

    public function getDataLabelForStatusSms()
    {
        if ($this->isSmsSend) {
            return 'Подтвердить телефон';
        }

        return 'Отправлен код проверки по СМС';
    }
}