<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use yii\helpers\ArrayHelper;
use frontend\modules\raffle\models\Check;
use yii\db\Expression;
use frontend\modules\raffle\models\Store;
use yii\web\UploadedFile;
use DateTime;

class RegisterCheckStep2Form extends Model
{

    public $storeId;
    public $numberCheck;
    public $dateCheck;
    public $imageCheck;
    public $captcha;
    public $agreeRule;

    public function init()
    {
        parent::init();
        if (!$this->storeId) {
            $currentStore = array_shift($this->getStores());
            if ($currentStore) {
                $this->storeId = $currentStore->id;
            }
        }
    }

    public function rules()
    {
        return [
            [['storeId', 'numberCheck', 'dateCheck', 'agreeRule'], 'required'],
            [['agreeRule'], 'compare', 'compareValue' => 1, 'message' => 'Подтвердите соглашения'],
            //[['imageCheck'], 'safe'],
            [['imageCheck'], 'file', 'extensions' => 'png,jpeg,jpg,bmp,pdf', 'skipOnEmpty' => FALSE],
            ['captcha', 'captcha', 'captchaAction' => '/core/index/captcha'],
            [['dateCheck'], 'date', 'format' => 'php:d/m/Y H:i:s'],
            [['dateCheck'], 'validateMysqlDatetime'],
        ];
    }

    public function registerCheck()
    {
        $session  = Yii::$app->session;
        $userId   = (Yii::$app->user->isGuest) ? $session->get('current-user-id') : Yii::$app->user->identity->getId();

        $this->imageCheck = UploadedFile::getInstance($this, 'imageCheck');

        if (!$this->validate() || !$userId) {
            return FALSE;
        }

        $check                = new Check();
        $check->store_id      = $this->storeId;
        $check->number        = $this->numberCheck;
        $check->check_date    = Yii::$app->formatter->asDate(str_replace('/', '-', $this->dateCheck), 'php: Y-m-d H:i:s');
        $check->user_id       = $userId;
        $check->user_ip       = Yii::$app->request->userIp;
        $check->register_date = new Expression('NOW()');
        $fileName             = $this->getHashFile($this->imageCheck->baseName);
        $direcrorySave        = Yii::getAlias('@frontend/web/') . Check::getFilePath();

        if (!file_exists($direcrorySave)) {
            mkdir($direcrorySave, 0777);
        }

        if ($this->imageCheck->saveAs($direcrorySave . DIRECTORY_SEPARATOR . $fileName . '.' . $this->imageCheck->extension)) {
            $check->check_image = $fileName . '.' . $this->imageCheck->extension;
            $check->image_hash = $fileName;
        }

        if ($check->validate() && $check->save()) {
            if ($session->has('current-user-id')) {
                $session->remove('current-user-id');
            }

            return TRUE;
        }

        return FALSE;

    }

    private function getHashFile($nameFile)
    {
        while (TRUE) {
            $hash = md5($nameFile + time());

            $check = Check::find()
                                ->where(['image_hash' => $hash])
                                ->one();

            if (!$check) {
                return $hash;
            }
        }
    }

    public function getStores()
    {
        return ArrayHelper::map(Store::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all(), 'id', function($model){ return $model; });
    }

    public function attributeLabels()
    {
        return [
            'storeId' => 'Торговая сеть, в которой совершена покупка',
            'numberCheck' => 'Номер чека',
            'dateCheck' => 'Дата и время в чеке',
            'imageCheck' => 'Изображение чека',
            'captcha' => 'Введите символы с изображения',
            'agreeRule' => 'Я согласен с правилами'
        ];
    }

    public function validateMysqlDatetime($attribute, $params, $validator)
    {
        $startPointDate = strtotime('01-01-1970 00:00:00');
        $endPontDate    = strtotime(date('d-m-Y H:i:s'));
        $checkDate      = strtotime(str_replace('/', '-', $this->{$attribute}));

        if ($checkDate < $startPointDate || $checkDate > $endPontDate) {
            $message = (isset($params[ 'message' ])) ? $params[ 'message' ] : 'Не правильная дата';
            $this->addError($attribute, $message);
        }

        return TRUE;
    }
}