<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use yii\helpers\ArrayHelper;

class UserAddressForm extends Model
{

    public $address;
    public $address_name;
    public $addressIndex;
    public $addressFlat;

    public function rules()
    {
        return [
            [['address'], 'required'],
            [['address', 'addressIndex', 'addressFlat'], 'string'],
            [['address', 'addressIndex', 'addressFlat', 'address_name'], 'filter', 'filter' => 'trim'],
            [['address_name'], 'safe'],
        ];
    }

    public function saveAddress()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);

        $fieldsAddress = explode(',', $this->address_name);

        if (!preg_match('/^\d+$/', $fieldsAddress[ 0 ])) {
            if (!empty($this->addressFlat)) {
                $fieldsAddress[] = $this->addressFlat;
            }
            if (!empty($this->addressIndex)) {
                $fieldsAddress = ArrayHelper::merge([$this->addressIndex], $fieldsAddress);
            }
        }

        $user->updateAttributes(['address' => implode(',', $fieldsAddress)]);

        return TRUE;
    }

}