<?php
namespace frontend\modules\user\models\forms;

use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\user\models\Profile;

class SignupForm extends Model
{
    public $email;
    public $password;
    public $repeatPassword;
    public $login;

    public function rules()
    {
        return [
            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'unique', 'targetClass' => User::className(), 'message' => 'This login has already been taken.'],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'This email address has already been taken.'],

            [['password', 'repeatPassword'], 'required'],
            ['password', 'string', 'min' => 6],

            ['repeatPassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user         = new User();
        $user->login  = $this->login;
        $user->email  = $this->email;

        $user->setRegisterIp();
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save()) {
            $profile = new Profile();
            
            $user->link('profile', $profile);

            return $user;
        }

        return NULL;
        
    }
}
