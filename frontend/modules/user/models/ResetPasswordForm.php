<?php
namespace frontend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use frontend\modules\user\models\User;
use frontend\modules\user\models\forms\RegisterCheckStep1Form;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $phone;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['phone', 'required'],
        ];
    }

    public function resetPassword($password, $tpl)
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $phone = preg_replace('/\D/', '', $this->phone);

        $user = User::findByPhone(['phone' => $phone]);

        if (!$user) {
            $this->addError('phone', 'Пользователь не найден');
            return FALSE;
        }

        $user->setPassword($password);

        if ($user->save(FALSE)) {

            $result = RegisterCheckStep1Form::sendSmsGateway($tpl, Yii::$app->params[ 'sms' ][ 'host' ]);

            if ($result && $result->information == 'send') {
                return TRUE;
            }

        }

        return FALSE;
    }

    public static function generateNewPassword()
    {
        return Yii::$app->security->generateRandomString(12);
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон',
        ];
    }
}
