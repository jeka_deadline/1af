<?php
namespace frontend\modules\user\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\modules\core\components\FrontController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\user\models\forms\LoginForm;
use frontend\modules\user\models\ResetPasswordForm;
use yii\web\Response;
use frontend\modules\user\models\forms\RegisterCheckStep1Form;

class SecurityController extends FrontController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                return $this->goBack();
            }
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return [
                    'html' => $this->renderAjax('login-form', ['model' => $model]),
                ];
            }

            return $this->render('login', [
                'model' => $model,
            ]);
        } else {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['html' => $this->renderAjax('login-ajax', ['model' => $model])];
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionResetPassword()
    {
        $response = [
            'html' => NULL,
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;

        try {
            $model = new ResetPasswordForm();
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $newPassword = ResetPasswordForm::generateNewPassword();


        if ($model->load(Yii::$app->request->post())) {
            $phone = preg_replace('/\D/', '', $model->phone);

            $tpl = $this->renderPartial('@frontend/modules/raffle/views/sms/reset-password', [
                'newPassword' => $newPassword,
                'login'       => Yii::$app->params[ 'sms' ][ 'login' ],
                'password'    => Yii::$app->params[ 'sms' ][ 'password' ],
                'sendPhone'   => $phone,
            ]);

            if ($password = $model->resetPassword($newPassword, $tpl)) {

                $response[ 'html' ] = $this->renderPartial('success-reset-password');

            } else {
                $response[ 'html' ] = $this->renderPartial('reset-password-form', [
                    'model' => $model,
                ]);
            }

            return $response;

        }

        $response[ 'html' ] = $this->renderAjax('reset-password', [
            'model' => $model,
        ]);

        return $response;
    }

    public function actionGetGuestForm()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model              = new RegisterCheckStep1Form();
        $model->scenario    = RegisterCheckStep1Form::SCENARIO_LOGIN;
        $model->isRegister  = 1;

        return [
            'html' => $this->renderAjax('@app/modules/raffle/views/check-ajax/register-check-step1', [
                'model' => $model,
            ]),
        ];
    }

    public function actionUserBar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response['html'] = [
            'desktop'=>$this->renderAjax('user-bar/desktop'),
            'mobile'=>$this->renderAjax('user-bar/mobile'),
        ];

        return $response;
    }
}
