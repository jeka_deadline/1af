<?php
namespace frontend\modules\user\controllers;

use Yii;
use frontend\modules\core\components\FrontController;
use frontend\modules\user\models\forms\UserAddressForm;
use frontend\modules\user\models\forms\NotFoundUserAddressForm;
use yii\web\Response;
use yii\filters\AccessControl;

class PersonalAreaAjaxController extends FrontController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['save-address', 'modal-not-found-address'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionSaveAddress()
    {
        Yii::$app->response->format= Response::FORMAT_JSON;

        $response = [
            'html' => NULL,
            'reload' => FALSE,
        ];

        if (!Yii::$app->user->identity->isUserWinner()) {
            return $response;
        }

        $model = new UserAddressForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveAddress()) {
                $response[ 'html' ] = $this->renderPartial('success-save-address');
                $response[ 'reload' ] = TRUE;
            } else {
                $response[ 'html' ] = $this->renderAjax('@app/modules/user/widgets/UserAddress/views/user-address-form', [
                    'model' => $model,
                ]);
            }
        } else {
            $response[ 'html' ] = NULL;
        }

        return $response;
    }

    public function actionModalNotFoundAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'html' => NULL,
            'reload' => FALSE,
        ];

        if (!Yii::$app->user->identity->isUserWinner()) {
            return $response;
        }

        $model = new NotFoundUserAddressForm();

        if ($model->load(Yii::$app->request->post())) {
           if ($model->saveAddress()) {
              $response[ 'modal' ] = $this->renderPartial('success-save-address');
              $response[ 'reload' ] = TRUE;
           } else {
              $response[ 'html' ] = $this->renderPartial('@app/modules/user/widgets/UserAddress/views/user-not-found-address-form', [
                    'model' => $model,
                ]);
           }
        } else {
            $response[ 'html' ] = $this->renderPartial('@app/modules/user/widgets/UserAddress/views/user-not-found-address-modal', [
                'model' => $model,
            ]);
        }

        return $response;
    }

}