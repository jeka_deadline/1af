<?php
namespace frontend\modules\user\controllers;

use Yii;
use frontend\modules\core\components\FrontController;
use frontend\modules\user\models\User;
use yii\filters\AccessControl;
use frontend\modules\raffle\models\Check;
use yii\data\ArrayDataProvider;

class PersonalAreaController extends FrontController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $user = User::findOne(Yii::$app->user->identity->getId());

        $listWinnersChecks = Check::find()
                                        ->where(['user_id' => $user->id, 'status' => Check::STATUS_ACCEPT])
                                        ->andWhere(['not', ['prize_id' => NULL]])
                                        ->all();

        $listChecksPrizes = Check::find()
                            //->where(['not', ['prize_id' => NULL]])
                            ->andWhere(['user_id' => $user->id])
                            //->andWhere(['<>', 'status', Check::STATUS_PROCESSING])
                            ->all();

        $listWinnersChecksDataProvider = new ArrayDataProvider([
            'allModels' => $listWinnersChecks,
            'pagination' => [
                'pageSize' => 10,
                'params' => [
                    'page' => (Yii::$app->request->get('tab') !== 'check-prizes') ? Yii::$app->request->get('page') : 1,
                    'tab' => 'winner-check',
                ],
            ],
        ]);

        $listChecksPrizesDataProvider = new ArrayDataProvider([
            'allModels' => $listChecksPrizes,
            'pagination' => [
                'pageSize' => 10,
                'params' => [
                    'page' => (Yii::$app->request->get('tab') === 'check-prizes') ? Yii::$app->request->get('page') : 1,
                    'tab' => 'check-prizes',
                ],
            ],
        ]);

        return $this->render('index', [
            'user' => $user,
            'listWinnersChecksDataProvider' => $listWinnersChecksDataProvider,
            'listChecksPrizesDataProvider' => $listChecksPrizesDataProvider,
        ]);
    }

}