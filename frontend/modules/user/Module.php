<?php
namespace frontend\modules\user;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'frontend\modules\user\controllers';

    public function init()
    {
        return parent::init();
    }

}