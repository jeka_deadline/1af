<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'id' => 'modal-form-default',
    'header' => 'Авторизация',
]); ?>

    <?= $this->render('login-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>