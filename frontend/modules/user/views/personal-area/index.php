<?php
use yii\bootstrap\Tabs;
?>

<?= Tabs::widget([
    'items' => [
        [
            'label'   => 'Чеки и призы',
            'content' => $this->render('prizes'),
            'active'  => (Yii::$app->request->get('tab') === 'delivery') ? FALSE : TRUE,
        ],
        [
            'label'   => 'Доставка призов',
            'content' => $this->render('tab-delivery', [
            'listWinnersChecks' => $listWinnersChecks,
            ]),
            'active'  => (Yii::$app->request->get('tab') === 'delivery') ? TRUE : FALSE,
        ],
    ],
]) ;?>