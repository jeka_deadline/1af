<?php
use frontend\modules\user\widgets\UserAddress\UserAddress;
?>

<?= UserAddress::widget(); ?>

<h3>Статусы отправки призов</h3>

<?php if ($listWinnersChecks) : ?>

    <table class="table">
        <tr>
            <th>Название приза</th>
            <th>Статус</th>
            <th>Номер отслеживания (ШПИ)</th>
            <th></th>
        </tr>

        <?php foreach ($listWinnersChecks as $check) : ?>

            <tr>
                  <td><?= $check->prize->name; ?></td>
                  <td><?= ($check->sendStatus) ? $check->sendStatus->name : NULL; ?></td>
                  <td><?= $check->send_number; ?></td>
                  <td><?= $check->getLinkSend(); ?></td>
            </tr>

        <?php endforeach; ?>

    </table>

<?php else : ?>
<?php endif; ?>