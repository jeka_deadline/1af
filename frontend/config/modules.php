<?php
return [
    'core' => [
        'class' => 'frontend\modules\core\Module',
    ],
    'user' => [
        'class' => 'frontend\modules\user\Module',
    ],
    'raffle' => [
        'class' => 'frontend\modules\raffle\Module',
    ],
    'dictionary' => [
        'class' => 'frontend\modules\dictionary\Module',
    ],
    'page' => [
        'class' => 'frontend\modules\page\Module',
    ],
];
?>