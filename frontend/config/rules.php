<?php
return [
    '' => '/core/index/index',
    '/faq' => '/dictionary/faq/index',
    '/winners' => '/raffle/check/winners',
    '/cabinet' => '/user/personal-area/index',
    '/rules' => '/core/index/rules',
    '<uri:\w+>' => '/page/page/index',
];
?>