$(function() {

    $(document).on('change', '.b-winners .js-action-select', function(e) {
        var $form = $(this).closest('form');

        var uri = new Uri()
        .setPath(location.pathname)
        .setQuery('?' + $($form).serialize());

        location.href = uri.toString();
    })
})