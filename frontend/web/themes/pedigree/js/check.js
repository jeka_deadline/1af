$(function() {

    $(document).on('change', '[type=checkbox].js-is-register', function( e ) {
        var optionsMask = {
            clearIfNotMatch: true
        };
        var $form = $(this).closest('form');

        if ($(this).prop('checked')) {
            $(document).find('#input-login-password').closest('.inputs-row').removeClass('hidden');
            $(document).find('#btn-reset-password').removeClass('hidden');
            $(document).find('#block-sms').addClass('hidden');
            $(document).find('#block-register').addClass('hidden');
        } else {
            $.ajax({
                url: $($form).data('url'),
                method: 'POST',
                data: $($form).serialize(),
                dataType: 'JSON',
            }).done(function(data) {
                if (data.html) {
                    $($form).replaceWith(data.html);
                    $(document).find('.styled').styler();
                    initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                    initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);
                }
            });
        }
    });

    $(document).on('click', '#resend-sms', function( e ) {
        e.preventDefault();
        var optionsMask = {
            clearIfNotMatch: true
        };
        var $form = $(this).closest('form');

        $.ajax({
            url: $($form).data('url'),
            method: 'POST',
            data: $($form).serialize(),
            dataType: 'JSON',
        }).done(function(data) {
            if (data.html) {
                $($form).replaceWith(data.html);
                $(document).find('.styled').styler();
                initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);

                var $btnResendSms = $(document).find('#resend-sms');
                var $timer        = $(document).find('#timer');

                $($btnResendSms).css('display', 'none');
                $($timer).css({marginLeft: '5px', display: 'inline-block'});
                $($timer).text('смс отправлено');

                setTimeout(function(){
                    $($timer).text('00:59');
                    timer($timer, function(){
                        $($timer).text('');
                        $($btnResendSms).css('display', 'inline-block');
                    });
                }, 1000);
            }
        });
    });

    $(document).on('change', '[type=checkbox]#js-checkbox-is-send-sms', function( e ) {
        var $blockInputSms = $(document).find('#block-input-sms-code');
        var $spanStatusSms = $(document).find('#label-status-sms').get(0);
        var text = $($spanStatusSms).text();
        $($spanStatusSms).text($($spanStatusSms).data('label'));
        $($spanStatusSms).data('label', text);
        if ($(this).prop('checked')) {
            //$(document).find('#block-register').toggleClass('hidden');
            $($blockInputSms).removeClass('hidden');
        } else {
            $(document).find('#block-register').addClass('hidden');
            $($blockInputSms).addClass('hidden');
        }
    });

    $(document).on('submit', '#register-form-step2', function(e) {
        e.preventDefault();
        $(this).yiiActiveForm('validate');

        if ($(this).find('.error').length) {
            return false;
        }

        var self = $(this);
        var data = new FormData($(this).get(0));
        var optionsMask = {
            clearIfNotMatch: true
        };
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: data,
            dataType: 'JSON',
            method: $(this).attr('method'),
            contentType: false,
            cache: false,
            processData: false
        }).done(function(data) {
            if (data.html) {
                $(self).replaceWith(data.html);
                $(document).find('.styled').styler();
                initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);
            }
            if (data.modal) {
                $('.modal-1').replaceWith(data.modal);
                $(document).find('.styled').styler();
                initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);
                callFancybox();
            }
            if (data.reload) {
                setTimeout(function(){location.reload();}, 2000);
            }
        })
    });

});