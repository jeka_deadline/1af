$(function() {

    var optionsMask = {
        clearIfNotMatch: true
    };

    initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
    initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);

    $.ajax({
        url: '/raffle/check-ajax/check-user-age',
        method: 'get',
        dataType: 'JSON'
    }).done(function(data) {
        if (data.html) {
            $('.modal-1').replaceWith(data.html);
            $(document).find('.styled').styler();
            initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
            initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);
            callFancybox();
        }
    })

    $(document).on('submit', 'form.ajax-modal-form', function( e ) {
        var self = $(this);
        var data = new FormData($(this).get(0));
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: data,
            dataType: 'JSON',
            method: $(this).attr('method'),
            contentType: false,
            cache: false,
            processData: false
        }).done(function(data) {
            if (data.html) {
                $(self).replaceWith(data.html);
                $(document).find('.styled').styler();
                initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);
            }
            if (data.modal) {
                $('.modal-1').replaceWith(data.modal);
                $(document).find('.styled').styler();
                initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);

                if (data.showModal) {
                    callFancybox();
                }
            }
            if (data.reload) {
                setTimeout(function(){location.reload();}, 2000);
            }

            if (data.callback && (typeof window[data.callback] === "function")) {
                window[data.callback](data);
            }
        })
    });

    $(document).on('click', '.modal-open, .form-modal', function( e ) {
        e.preventDefault();
        $.ajax({
            'url': $(this).data('url'),
            'method': 'GET',
            'dataType': 'JSON',
        }).done(function(data) {
            if (data.html != '') {
                $.fancybox.close();
                initModal();
                $('.modal-1').replaceWith(data.html);
                $(document).find('.styled').styler();
                initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);
                callFancybox();
            }
        });
    });

    $(document).on('click', '.secondary-modal-open', function( e ) {
        e.preventDefault();
        $.ajax({
            'url': $(this).data('url'),
            'method': 'GET',
            'dataType': 'JSON',
        }).done(function(data) {
            if (data.html != '') {
                $('body').append(data.html);
                $('.secondary-modal').show();
                $('body').addClass('secondary-modal-active');
            }
        });
    });

    $(document).on('click', '.secondary-modal .modal-close, .secondary-modal .close-btn', function (e) {
        e.preventDefault();
        console.log('test');
        $(document).find('.secondary-modal').each(function() {
            $(this).remove();
        });
        $('body').removeClass('secondary-modal-active');
    });

    $(document).on('click', '.replace-modal-form', function(e) {
        e.preventDefault();
        $.ajax({
            'url': $(this).data('url'),
            'method': 'GET',
            'dataType': 'JSON',
        }).done(function(data) {
            if (data.html != '') {
                $.fancybox.close();
                initModal();
                $('.modal-1').replaceWith(data.html);
                $(document).find('.styled').styler();
                initMask($(document).find('.phone-input'), '+7(999) 999-9999', optionsMask);
                initMask($(document).find('.datetime-input'), '00/00/0000 00:00:00', optionsMask);
                callFancybox();
            }
        });
    });
});

function callFancybox()
{
    $.fancybox({
        content: $(document).find('.modal-1').show(),
        modal: true,
        autoSize: true,
        type: 'inline',
        closeBtn: false,
        padding: 0,
        margin: 10,
        scrolling: 'visible',
        fixed: false,
        autoCenter: false,
        afterClose: function() {
            initModal();
        },
    });
}

function initModal()
{
    $(document).find('.modal-1').each(function() {
        $(this).remove();
    });
    if (!$(document).find('.modal-1').length) {
        $('body').append('<div id="modal_modals" class="modal modal-1"></div>');
    }
}

function initMask(object, mask, options)
{
    if ($(object).length) {
        $(object).mask(mask, options);
    }
}

function timer(obj, callback) {
    var time = $(obj).text();
    var arr = time.split(":");
    var m = arr[0];
    var s = arr[1];
    if (s == 0) {
      if (m == 0) {
        callback();
        return;
      }
      m--;
      if (m < 10) m = "0" + m;
      s = 59;
    }
    else s--;
    if (s < 10) s = "0" + s;
    $(obj).text(m + ":" + s);
    setTimeout(timer, 1000, obj, callback);
}