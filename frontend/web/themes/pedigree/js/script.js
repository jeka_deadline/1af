function homescreen() {
	var w_h = $(window).height();
	var w_w = $(window).width();
	var s_h = $('.b-steps').outerHeight();
	var hs = $('.b-homescreen');
	var delta = w_h;

	delta = w_h - s_h - parseInt(hs.css('padding-top')) - parseInt(hs.css('padding-bottom'));
	if (delta > 100 && delta <= 537 && w_w > 1279) {
		hs.css('height', delta + 'px');
	}
}

function scroll_overlay() {
	var vScroll = $(window).scrollTop() + $(window).height();
	$('.scroll-overlay').each(function() {
		var obj = $(this);
		if (vScroll > obj.offset().top) {
			setTimeout(function() {
				obj.removeClass('scroll-overlay');
			}, 3000);
		}
	});
}

function callbackPanelUserAuthorized(data) {
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "/user/security/user-bar",
        success: function(response){
            $('header .userbar').html($(response.html.desktop).html());
            $('.mobile-menu .userbar').html($(response.html.mobile).html());
        }
    });
}

$(window).on('load resize', function() {
	homescreen();
	scroll_overlay();
});

$(window).on('scroll', function() {
	scroll_overlay();
});

$(document).on('ready', function () {
	homescreen();

	$('.styled').styler();

	/*$('.modal-open').fancybox({
		autoSize: true,
		type: 'inline',
		closeBtn: false,
		padding: 0,
		margin: 10,
		scrolling: 'visible',
		fixed: false,
		autoCenter: false
	});*/
	
	$(document).on('click', '.secondary-modal-open', function() {
		$($(this).attr('href')).addClass('active');
		$('body').addClass('secondary-modal-active');
		return false;
	});

	$(document).on('click', '.fancybox-wrap .modal-close, .fancybox-wrap .close-btn', function () {
		$.fancybox.close();
		return false;
	});
	
	$(document).on('click', '.secondary-modal .modal-close, .secondary-modal .close-btn', function () {
		$(this).closest('.secondary-modal').removeClass('active');
		$('body').removeClass('secondary-modal-active');
		return false;
	});

	$(document).on('click', '.menu-btn', function () {
		$('body').toggleClass('menu-active');
		return false;
	});

	// Cabinet
	$(document).on('click', '.b-cabinet .panel .tabs li a', function () {
		var a = $(this);
		var tab = a.closest('li');
		$('.b-cabinet .panel .tabs li, .b-cabinet .panel .tab, .b-cabinet .pagination ul').removeClass('active');
		$('.b-cabinet .panel').find(a.attr('href')).addClass('active');
		$('.b-cabinet .pagination').find(a.attr('href') + '_pagination').addClass('active');
		tab.addClass('active');
		return false;
	});
	// prizes
	$(document).on('click', '.b-prizes .panel .tabs li a', function () {
		var tab = $(this).closest('li');
		$('.b-prizes .panel .tabs li, .b-prizes .panel .tab').removeClass('active');
		$('.b-prizes .panel .tab').eq(tab.index()).addClass('active');
		tab.addClass('active');
		return false;
	});
	// answers
	$(document).on('click', '.b-faq .panel .tabs li a', function () {
		var tab = $(this).closest('li');
		$('.b-faq .panel .tabs li, .b-faq .panel .tab').removeClass('active');
		$('.b-faq .panel .tab').eq(tab.index()).addClass('active');
		tab.addClass('active');
		return false;
	});
	$(document).on('click', '.b-faq .panel .faq-link', function () {
		$(this).next().slideToggle();
		$(this).toggleClass('faq-link--active');
	});

	$(document).on('focus keydown', '.input-1 input, .textarea-1 textarea', function () {
		$(this).parent().removeClass('error');
	});
});