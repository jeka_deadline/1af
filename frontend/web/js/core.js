$(function() {

    $(document).on('submit', 'form.ajax-modal-form', function( e ) {
        var self = $(this);
        var modal = $(this).closest('.modal-body');
        var data = new FormData($(this).get(0));
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: data,
            dataType: 'JSON',
            method: $(this).attr('method'),
            contentType: false,
            cache: false,
            processData: false
        }).done(function(data) {
            if (data.html) {
                $(self).replaceWith(data.html);
            }
            if (data.modal) {
                $(modal).empty().append(data.modal);
            }
            if (data.reload) {
                setTimeout(function(){location.reload();}, 2000);
            }
        })
    });

    $(document).on('click', '.get-modal', function( e ) {
        e.preventDefault();
        $.ajax({
            'url': $(this).data('url'),
            'method': 'GET',
            'dataType': 'JSON',
        }).done(function(data) {
            if (data.html != '') {
                $('#modal-form-default').replaceWith(data.html);
            }
            $('#modal-form-default').modal();
        });
    });

})