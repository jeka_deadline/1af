<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class WinnerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/pedigree/';
    public $css = [
    ];
    public $js = [
        'js/uri.js',
        'js/winner.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
      'forceCopy'=>true,
    ];
}
