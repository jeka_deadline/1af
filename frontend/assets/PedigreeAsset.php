<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class PedigreeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/pedigree/';
    public $css = [
        'style/fonts/style.css',
        'js/formstyler/jquery.formstyler.css',
        'js/fancybox/jquery.fancybox.css',
        'style/style.css',
    ];
    public $js = [
        'js/formstyler/jquery.formstyler.min.js',
        'js/fancybox/jquery.fancybox.pack.js',
        'js/script.js',
        //'js/mask-input.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js',
        'js/core.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
      'forceCopy'=>true,
    ];

    public function init()
    {
        parent::init();
    }
}
