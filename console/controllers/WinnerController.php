<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\raffle\Check;
use common\models\raffle\TabletWinnerDate;
use DateTime;

class WinnerController extends Controller
{

    public function actionGetBackpackWinner()
    {
        Yii::info(str_repeat('=', 50), 'winner');
        $currentDate  = date('Y-m-d');
        $date         = new DateTime($currentDate);

        $date->modify('-1 day');

        $winnerDate = $date->format('Y-m-d');

        Yii::info(date('d-m-Y H:i:s') . " Начинаем розыгрыш приза Рюкзак за дату = {$currentDate}", 'winner');

        $check = Check::find()
                            ->where(['DATE(winner_date)' => $currentDate])
                            ->one();

        if ($check) {
            Yii::info("Розыгрыш приза Рюкзак за дату = {$currentDate} закончен", 'winner');
            return FALSE;
        }

        $query = Check::find()
                          ->andWhere(['between', 'register_date', $winnerDate, $currentDate])
                          ->orderBy(['register_date' => SORT_ASC]);

        $countChecks  = $query->count();

        Yii::info("Получаем список призов и идем по нему циклом", 'winner');
        $hashId = md5(time() . Yii::$app->security->generateRandomString());

        Yii::info("Разыгрываем приз с HASH ID {$hashId}", 'winner');
        Yii::info("Чеков зарегистрировано: {$countChecks}", 'winner');

        if (!$countChecks) {
            return FALSE;
        }
        $checks       = $query->all();
        $winner       = ($countChecks * (1 + tan($countChecks) + $countChecks)) % $countChecks;

        while ($checks) {
            if (isset($checks[ $winner ])) {
                $currentCheck = $checks[ $winner ];
                unset($checks[ $winner ]);
                if ($currentCheck->prize_id) {
                    Yii::info("Юзер с чеком с ID {$currentCheck->id} уже получал приз 1 раз", 'winner');
                    $winner++;
                    continue;
                }
                $currentCheck->winner_date = $currentDate;
                $currentCheck->prize_id    = Check::BACKPACK_PRIZE_ID;
                $currentCheck->hash_id     = $hashId;

                $currentCheck->save(FALSE);

                Yii::info("Победил чек с номером: {$winner} с ID {$currentCheck->id}", 'winner');
                return FALSE;
            } else {
                if (!$checks) {
                    return FALSE;
                }
                $winner = 0;
            }
        }
    }

    public function actionGetTabletWinner()
    {
        Yii::info(str_repeat('=', 50), 'winner');
        $currentDate = date('Y-m-d');

        Yii::info(date('d-m-Y H:i:s') . " Начинаем розыгрыш приза Планшет за дату = {$currentDate}", 'winner');

        $winnerDate = TabletWinnerDate::find()
                                            ->where(['date' => $currentDate])
                                            ->one();

        if (!$winnerDate || $winnerDate->is_winner) {
            Yii::info("Розыгрыш приза Планшет за дату = {$currentDate} закончен или в этот день не происходит", 'winner');
            return FALSE;
        }

        $query = Check::find()
                            ->orderBy(['register_date' => SORT_ASC]);

        $prevWinnerDate = TabletWinnerDate::find()
                                                ->where(['<', 'date', $currentDate])
                                                ->orderBy(['date' => SORT_ASC])
                                                ->one();

        if ($prevWinnerDate) {
            $query->andWhere(['between', 'register_date', $prevWinnerDate->date, $currentDate]);
        }

        $countChecks  = $query->count();

        Yii::info("Получаем список призов и идем по нему циклом", 'winner');
        $hashId = md5(time() . Yii::$app->security->generateRandomString());

        Yii::info("Разыгрываем приз с HASH ID {$hashId}", 'winner');
        Yii::info("Чеков зарегистрировано: {$countChecks}", 'winner');

        if (!$countChecks) {
            return FALSE;
        }
        $checks       = $query->all();
        $winner       = ($countChecks * (1 + tan($countChecks) + $countChecks)) % $countChecks;

        while ($checks) {
            if (isset($checks[ $winner ])) {
                $currentCheck = $checks[ $winner ];
                unset($checks[ $winner ]);
                if ($currentCheck->prize_id) {
                    Yii::info("Юзер с чеком с ID {$currentCheck->id} уже получал приз 1 раз", 'winner');
                    $winner++;
                    continue;
                }
                $currentCheck->winner_date = $currentDate;
                $currentCheck->prize_id    = Check::TABLET_PRIZE_ID;
                $currentCheck->hash_id     = $hashId;

                $currentCheck->save(FALSE);
                $winnerDate->updateAttributes(['is_winner' => 1]);

                Yii::info("Победил чек с номером: {$winner} с ID {$currentCheck->id}", 'winner');
                return FALSE;
            } else {
                if (!$checks) {
                    return FALSE;
                }
                $winner = 0;
            }
        }
    }

}
?>