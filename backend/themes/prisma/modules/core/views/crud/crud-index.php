<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>

<?= $headerContent; ?>

<div class="crud-index">

    <?php if ($showAddButton) : ?>

        <p>
            <?= Html::a(Yii::t('core', "Create"), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

    <?php endif; ?>

    <?= GridView::widget([

        'dataProvider'  => $dataProvider,
        'filterModel'   => $searchModel,
        'columns'       => $columns,

    ]); ?>

</div>

<?= $footerContent; ?>