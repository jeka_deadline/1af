<?php
use yii\helpers\Html;
?>

<?= $headerContent; ?>

<div>

    <?= $this->render('crud-form', [
        'model'             => $model,
        'formElements'      => $formElements,
        'activeFormConfig'  => $activeFormConfig,
    ]) ?>

</div>

<?= $footerContent; ?>