<?php
use backend\modules\user\models\forms\CreateUserForm;
?>

<?= $form->field($model, 'phone')->textInput(); ?>

<?= $form->field($model, 'email')->textInput(); ?>

<?= $form->field($model, 'fullName')->textInput(); ?>

<?= $form->field($model, 'address')->textarea(); ?>

<?= $form->field($model, 'password')->textInput(); ?>

<?php if ($model->getScenario() === CreateUserForm::SCENARIO_CREATE) : ?>

    <?= $form->field($model, 'repeatPassword')->textInput(); ?>

    <?= $form->field($model, 'isAdmin')->checkbox(); ?>

    <?= $form->field($model, 'isConfirmEmail')->checkbox(); ?>

    <?= $form->field($model, 'isConfirmPhone')->checkbox(); ?>

<?php endif; ?>