<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';

//print_r(); exit;
?>

<div class="container">
      <div class="row">
          <div class="col-xs-12">
              <div id="login-box">
                  <div id="login-box-holder">
                      <div class="row">
                          <div class="col-xs-12">
                              <header id="login-header">
                                  <div id="login-logo">
                                      <img src="<?= $this->theme->getUrl('img/logo.png'); ?>" alt=""/>
                                  </div>
                              </header>
                              <div id="login-box-inner">

                                  <?php $form = ActiveForm::begin([
                                      'id' => 'login-form',
                                      'fieldConfig' => [
                                          'options' => [
                                              'class' => 'input-group',
                                          ],
                                      ],
                                  ]); ?>

                                      <?= $form->field($model, 'email', [
                                          'template' => "<span class='input-group-addon'><i class='fa fa-user'></i></span>{input}",
                                      ])->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

                                      <?= Html::error($model, 'email'); ?>

                                      <?= $form->field($model, 'password',[
                                          'template' => "<span class='input-group-addon'><i class='fa fa-key'></i></span>{input}",
                                      ])->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

                                      <?= Html::error($model, 'password'); ?>

                                      <?= $form->field($model, 'rememberMe', [
                                          'options' => [
                                              'id' => 'remember-me-wrapper',
                                              'class' => NULL,
                                          ],
                                          'checkboxTemplate' => '<div class="row"><div class="col-xs-6"><div class="checkbox-nice">{input}{label}</div></div></div>',
                                      ])->checkbox() ?>

                                      <div class="row">
                                          <div class="col-xs-12">

                                              <?= Html::submitButton('Войти', ['class' => 'btn btn-success col-xs-12', 'name' => 'login-button']) ?>

                                          </div>
                                      </div>

                                  <?php ActiveForm::end(); ?>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>