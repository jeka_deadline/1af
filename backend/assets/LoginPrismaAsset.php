<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class LoginPrismaAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/prisma/';
    //public $sourcePath = '@app/themes/prisma/assets';

    public $css = [
        'css/libs/font-awesome.css',
        'css/libs/nanoscroller.css',
        'css/compiled/theme_styles.css',
        '//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300',
    ];
    public $js = [
        'js/bootstrap.js',
        'js/jquery.nanoscroller.min.js',
        'js/demo.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions[ 'forceCopy' ] = TRUE;
    }
}
