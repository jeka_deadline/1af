$(function() {

    $(document).on('change', '#js-select-ready-answer', function(e) {
        var readyAnswerVal = $(this).val();

        if (!readyAnswerVal) {
            $('#textarea-answer').val('');
            return false;
        }

        $.ajax({
            url: $(this).data('url'),
            method: 'GET',
            data: {idReadyAnswer: readyAnswerVal},
        }).done(function(data){
            $('#textarea-answer').val(data);
        });
    })

})