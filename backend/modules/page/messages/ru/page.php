<?php
    return [
        'Management static pages' => 'Управление статическими страницами',
        'List static pages' => 'Список статических страниц',
        'Add static page' => 'Добавить статическую страницу',
        'Update static page' => 'Обновить статическую страницу',
        'View static page' => 'Просмотр статической страницы',

        'Page header' => 'Заголовок страницы',
        'Page content' => 'Содержание страницы',
        'Choose parent page' => 'Выберите родительскую страницу',
        'Parent' => 'Родительская страница',
    ];
?>