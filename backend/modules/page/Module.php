<?php
namespace backend\modules\page;

use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    public $controllerNamespace = 'backend\modules\page\controllers';
    public $defaultRoute = 'index';
    public $menuOrder = 25;

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations[ 'page' ])) {
            Yii::$app->i18n->translations[ 'page' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/page/messages',
            ];
        }
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => Yii::t('page', 'Management static pages'),
                'url'   => ['/page/page/index'],
                'icon' => '<i class="fa fa-question" aria-hidden="true"></i>',
            ]
        ];
    }

}