<?php

namespace backend\modules\page\models;

use Yii;
use common\models\page\Page as BasePage;
use yii\helpers\ArrayHelper;
use common\models\core\PageMeta;
use yii\helpers\Html;
use common\models\core\Helper as CommonHelper;

class Page extends BasePage
{

    public $meta_title;
    public $meta_description;
    public $meta_keywords;
    public static $tree;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
                [['meta_description', 'meta_keywords'], 'string'],
                [['meta_title'], 'string', 'max' => 255],
                [['uri'], 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z\d_-]*[a-zA-Z\d]$/'],
            ]
        );
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'meta_title'        => Yii::t('core', 'Meta title'),
                'meta_description'  => Yii::t('core', 'Meta description'),
                'meta_keywords'     => Yii::t('core', 'Meta keywords'),
                'display_order'     => Yii::t('core', 'Display order'),
                'active'            => Yii::t('core', 'Active'),
                'uri'               => Yii::t('core', 'URI'),
                'menu_class'        => Yii::t('core', 'Page menu class'),
                'header'            => Yii::t('page', 'Page header'),
                'content'           => Yii::t('page', 'Page content'),
                'parent_id'         => Yii::t('page', 'Parent'),
            ]
        );
    }

    public function getFormElements()
    {
        $pages = self::getListPages(0, 0, $this->id);

        if (!$pages) {
            $pages = [];
        }

        $meta = $this->getMeta();

        if ($meta) {
            $this->meta_title       = $meta->meta_title;
            $this->meta_description = $meta->meta_description;
            $this->meta_keywords    = $meta->meta_keywords;
        }
        return [
            'main' => [
                'type' => 'tabs',
                'itemTabs' => [
                    [
                        'title'     => Yii::t('core', 'Page information'),
                        'active'    => TRUE,
                        'elements'  => [
                            'parent_id'     => [
                                                  'type'        => 'dropdownlist',
                                                  'items'       => $pages,
                                                  'attributes'  => ['prompt' => Yii::t('page', 'Choose parent page')]
                                              ],
                            'header'        => ['type' => 'text', 'fieldAttributes' => ['template' => '{label}{input}']],
                            'uri'           => ['type' => 'text'],
                            'content'       => [
                                                    'type' => 'widget', 'nameWidget' => '\zxbodya\yii2\tinymce\TinyMce',
                                                    'attributes' =>  [
                                                        'options' => ['rows' => '10'],
                                                        'language' => substr(Yii::$app->language, 0, 2),
                                                        'settings' => [
                                                            'plugins' => [
                                                                "advlist autolink lists link charmap print preview anchor",
                                                                "searchreplace visualblocks code fullscreen",
                                                                "insertdatetime media table contextmenu paste code fullscreen image textcolor preview media"
                                                            ],
                                                            'forced_root_block' => FALSE,
                                                        ],
                                                        'fileManager' => [
                                                            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                                                            'connectorRoute' => 'page/connector',
                                                        ],
                                                    ]
                                                ],
                            'menu_class'    => ['type' => 'text'],
                            'display_order' => ['type' => 'text'],
                            'active'        => ['type' => 'checkbox'],
                        ],
                    ],
                    [
                        'title'     => Yii::t('core', 'Seo information'),
                        'active'    => FALSE,
                        'elements'  => [
                            'meta_title'        => ['type' => 'text'],
                            'meta_description'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'meta_keywords'     => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                        ],
                    ],
                ],
            ],
        ];
    }

    public static function getListPages($pid = 0, $level = 0, $currentId = 0)
    {
        $pages = self::find()->where(['=', 'parent_id', $pid])->all();
        $level      = ($pid == 0) ? 0 : ++$level;
        foreach ($pages as $page) {
            if ($page->id == $currentId) {
                continue;
            }
            self::$tree[ $page->id ] = str_repeat('-', $level * 2) . $page->header;

            self::getListPages($page->id, $level, $currentId);
        }
        return self::$tree;
    }

    public function getViewAttributes()
    {
        return ['id', 'uri', 'header', 'display_order', 'active'];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->parent_id) {
                if ($parent = self::findOne($this->parent_id)) {
                    $this->full_uri = $parent->full_uri . '/' . $this->uri;
                } else {
                    $this->full_uri   = '/' . $this->uri;
                    $this->parent_id  = 0;
                }
            } else {
                $this->full_uri   = '/' . $this->uri;
                $this->parent_id  = 0;
            }

            return TRUE;
        }

        return FALSE;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveMeta();

        if ($this->parent) {
            $this->updateFullUri($this->children);
        }
    }

    private function updateFullUri($children)
    {
        foreach ($children as $child) {
            $child->updateAttributes(['full_uri' => $this->parent->full_uri . '/' . $this->uri, 'parent_id' => $this->parent->id]);
            $this->updateFullUri($child->children);
        }
    }

    private function saveMeta()
    {
        $meta = $this->getMeta();

        if (!$meta) {
            $meta = new PageMeta();
        }

        $meta->owner_id         = $this->id;
        $meta->modelClass      = CommonHelper::generateShortModelClass(get_class($this));
        $meta->meta_title       = $this->meta_title;
        $meta->meta_description = $this->meta_description;
        $meta->meta_keywords    = $this->meta_keywords;

        $meta->save(FALSE);
    }

    private function getMeta()
    {
        return CommonHelper::getPageMetaForModel($this->id, get_class($this));
    }


    // метод который генерирует шаблон для списка чекбоксов
    public function getTemplateForCheckboxList($index, $label, $name, $checked, $value)
    {
        return "<div class='checkbox'><label>" . Html::checkbox($name, $checked, ['value' => $value]) . "{$label}</label></div>";
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

}
