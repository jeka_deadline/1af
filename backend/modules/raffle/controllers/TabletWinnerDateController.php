<?php
namespace backend\modules\raffle\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class TabletWinnerDateController extends BackendController
{

    public $modelName   = 'backend\modules\raffle\models\TabletWinnerDate';
    public $searchModel = 'backend\modules\raffle\models\searchModels\TabletWinnerDateSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => 'Список дат разыгрывания планшета',
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => 'Добавить дату разыгрывания планшета',
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            [
                'attribute' => 'date',
                'filter' => FALSE,
            ],
            ['attribute' => 'is_winner'],
            $this->getGridActions(['template' => '{delete}']),
        ];
    }

}