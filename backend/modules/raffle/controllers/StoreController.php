<?php
namespace backend\modules\raffle\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class StoreController extends BackendController
{

    public $modelName   = 'backend\modules\raffle\models\Store';
    public $searchModel = 'backend\modules\raffle\models\searchModels\StoreSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete', 'delete-image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => 'Список торговых точек',
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => 'Добавить торговую точку',
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'backend\modules\core\components\CRUDUpdate',
                'title'     => 'Обновить торговую точку',
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'     => 'backend\modules\core\components\CRUDView',
                'title'     => 'Просмотр торговой точки',
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function actionDeleteImage($id)
    {
        $model = call_user_func_array([$this->modelName, 'findOne'], ['id' => $id]);

        if (!$model) {
            throw new NotFoundHttpException('Торговая точка не найдена');
        }

        $file = Yii::getAlias('@frontend/web/') . call_user_func([$this->modelName, 'getFilePath']) . '/' . $model->image;

        if (file_exists($file) && is_file($file) && is_writable($file)) {
            unlink($file);
            $model->updateAttributes(['image' => NULL]);
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute' => 'name'],
            ['attribute' => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),
        ];
    }

}