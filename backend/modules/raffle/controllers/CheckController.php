<?php
namespace backend\modules\raffle\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use backend\modules\raffle\models\Check;
use backend\modules\raffle\models\Store;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\helpers\Html;
use backend\modules\raffle\models\Prize;
use yii\helpers\ArrayHelper;
use backend\modules\raffle\models\SendStatus;

class CheckController extends BackendController
{

    public $modelName   = 'backend\modules\raffle\models\Check';
    public $searchModel = 'backend\modules\raffle\models\searchModels\CheckSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'change-check-status', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => 'Список зарегистрированных чеков',
                'showAddButton' => FALSE,
            ],
        ];
    }

    public function actionUpdate($id)
    {
        $model = call_user_func_array([$this->modelName, 'findOne'], ['id' => $id]);

        if (!$model) {
            throw new NotFoundHttpException('Чек не найден');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->prize_id && !$model->getOldAttribute('status') && $model->getAttribute('status') == Check::STATUS_REJECT) {
                $model->getNewWinner();
            } else {
                $model->save();
            }
            return $this->redirect(['index']);
        }

        return $this->render('@backend/modules/core/views/crud/crud-share-template-create-update', [
            'footerContent'     => NULL,
            'showUpdateButton'  => FALSE,
            'showDeleteButton'  => FALSE,
            'model'             => $model,
            'formElements'      => $model->getFormElements(),
            'activeFormConfig'  => [],
            'headerContent'     => $this->renderPartial('@backend/modules/core/views/crud/crud-view', [
                'headerContent'     => NULL,
                'showUpdateButton'  => NULL,
                'showDeleteButton'  => NULL,
                'model'             => $model,
                'footerContent'     => NULL,
            ]),
        ]);
    }

    public function actionChangeCheckStatus($id)
    {
        if (!Yii::$app->request->isPost) {
            return $this->redirect(['view', 'id' => $id]);
        }

        $model = call_user_func_array([$this->modelName, 'findOne'], ['id' => $id]);

        if (!$model) {
            throw new NotFoundHttpException('Чек не найден');
        }

        $model->load(Yii::$app->request->post());
        $model->save(FALSE);

        return $this->redirect(['view', 'id' => $id]);
    }

    public function getColumns()
    {
        $listStatuses = call_user_func([$this->modelName, 'getListStatuses']);
        $listPrizes = ArrayHelper::map(Prize::find()->all(), 'id', 'name');
        $listSendStatuses = ArrayHelper::map(SendStatus::find()->all(), 'id', 'name');

        return [
            $this->getGridSerialColumn(),
            [
                'attribute' => 'store_id',
                'value' => function($model){ return $model->store->name; },
                'filter' => Store::generateListStores(),
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model){ return $model->user->full_name; },
                'filter' => FALSE,
            ],
            ['attribute' => 'number'],
            ['attribute' => 'check_date'],
            [
                'attribute' => 'check_image',
                'value' => function($model){ return Html::a('Смотреть изображение', '/' . call_user_func([$model, 'getFilePath']) . '/' . $model->check_image, ['target' => '_blank']); },
                'format' => 'raw',
            ],
            [
                'attribute' => 'prize_id',
                'value' => function($model){ return ($model->prize) ? $model->prize->name : '-';},
                'filter' => $listPrizes,
            ],
            [
                'attribute' => 'winner_date',
                'filter' => FALSE,
            ],
            [
                'attribute' => 'status',
                'value' => function($model){ return $model->getStringStatus();},
                'filter' => $listStatuses,
            ],
            [
                'attribute' => 'hash_id',
            ],
            [
                'attribute' => 'send_status_id',
                'value' => function($model){ return $model->getSendStatusString();},
                'filter' => $listSendStatuses,
            ],
            $this->getGridActions(['template' => '{update}']),
        ];
    }

}

?>