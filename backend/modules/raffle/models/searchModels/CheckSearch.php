<?php

namespace backend\modules\raffle\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\raffle\models\Check;

/**
 * CheckSearch represents the model behind the search form about `backend\modules\raffle\models\Check`.
 */
class CheckSearch extends Check
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_id', 'user_id', 'status', 'prize_id'], 'integer'],
            [['number', 'check_date', 'check_image', 'register_date', 'reason_rejection', 'winner_date', 'user_ip', 'image_hash'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Check::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_id' => $this->store_id,
            'user_id' => $this->user_id,
            'check_date' => $this->check_date,
            'register_date' => $this->register_date,
            'status' => $this->status,
            'prize_id' => $this->prize_id,
            'winner_date' => $this->winner_date,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'check_image', $this->check_image])
            ->andFilterWhere(['like', 'reason_rejection', $this->reason_rejection])
            ->andFilterWhere(['like', 'user_ip', $this->user_ip])
            ->andFilterWhere(['like', 'image_hash', $this->image_hash]);

        return $dataProvider;
    }
}
