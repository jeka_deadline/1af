<?php
namespace backend\modules\raffle\models;

use Yii;
use common\models\raffle\SendStatus as BaseSendStatus;

class SendStatus extends BaseSendStatus
{

    public function getFormElements()
    {
        $fields =  [
            'name' => ['type' => 'text'],
        ];

        return $fields;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
        ];
    }

}