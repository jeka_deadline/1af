<?php

use yii\db\Migration;

class m170622_064611_add_column_ip_check extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%raffle_check}}', 'user_ip', $this->string(15)->notNull());
        $this->addColumn('{{%raffle_check}}', 'image_hash', $this->string(32)->notNull());
    }

    public function safeDown()
    {
        echo "m170622_064611_add_column_ip_check cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170622_064611_add_column_ip_check cannot be reverted.\n";

        return false;
    }
    */
}
