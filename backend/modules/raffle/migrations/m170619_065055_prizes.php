<?php

use yii\db\Migration;

class m170619_065055_prizes extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%raffle_prizes}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'image' => $this->string(255)->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active' => $this->smallInteger(1)->defaultValue(0),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%raffle_prizes}}');
    }
}
