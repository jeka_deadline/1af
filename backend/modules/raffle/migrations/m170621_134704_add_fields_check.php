<?php

use yii\db\Migration;

class m170621_134704_add_fields_check extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%raffle_check}}', 'prize_id', $this->integer(11)->defaultValue(NULL));
        $this->addColumn('{{%raffle_check}}', 'winner_date', $this->timestamp()->defaultValue(NULL));

        $this->createRelations();
    }

    public function safeDown()
    {
        echo "m170621_134704_add_fields_check cannot be reverted.\n";

        return false;
    }

    private function createRelations()
    {
        $this->createIndex('ix_raffle_check_prize_id', '{{%raffle_check}}', 'prize_id');
        $this->addForeignKey('fk_raffle_check_prize_id', '{{%raffle_check}}', 'prize_id', '{{%raffle_prizes}}', 'id', 'SET NULL', 'CASCADE');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170621_134704_add_fields_check cannot be reverted.\n";

        return false;
    }
    */
}
