<?php

use yii\db\Migration;

class m170622_092834_tablet_winner_steps extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%raffle_tablet_winner_date}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'is_winner' => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createIndex('uix_raffle_tablet_winner_date_date', '{{%raffle_tablet_winner_date}}', 'date', TRUE);
    }

    public function safeDown()
    {
        $this->dropTable('{{%raffle_tablet_winner_date}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170622_092834_tablet_winner_steps cannot be reverted.\n";

        return false;
    }
    */
}
