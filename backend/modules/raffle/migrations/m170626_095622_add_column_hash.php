<?php

use yii\db\Migration;

class m170626_095622_add_column_hash extends Migration
{
    public function safeUp()
    {

        $this->createTable('{{%raffle_send_statuses}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);

        $this->addColumn('{{%raffle_check}}', 'hash_id', $this->string(32)->defaultValue(NULL));
        $this->addColumn('{{%raffle_check}}', 'send_status_id', $this->integer(11)->defaultValue(NULL));
        $this->addColumn('{{%raffle_check}}', 'send_number', $this->string(50)->defaultValue(NULL));

        $this->createIndex('ix_raffle_check_send_status_id', '{{%raffle_check}}', 'send_status_id');
        $this->addForeignKey('fk_raffle_check_send_status_id', '{{%raffle_check}}', 'send_status_id', '{{%raffle_send_statuses}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170626_095622_add_column_hash cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170626_095622_add_column_hash cannot be reverted.\n";

        return false;
    }
    */
}
