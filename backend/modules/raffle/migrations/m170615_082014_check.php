<?php

use yii\db\Migration;
use yii\db\Expression;

class m170615_082014_check extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%raffle_store}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'image' => $this->string(255)->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active' => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createTable('{{%raffle_check}}', [
            'id'            => $this->primaryKey(),
            'store_id'      => $this->integer(11)->notNull(),
            'user_id'       => $this->integer(11)->notNull(),
            'number'        => $this->string(50)->notNull(),
            'check_date'    => $this->timestamp()->notNull(),
            'check_image'   => $this->text()->defaultValue(NULL),
            'register_date' => $this->timestamp(),
        ]);

        $this->createRelations();
    }

    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%raffle_store}}');
        $this->dropTable('{{%raffle_check}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_raffle_check_store_id', '{{%raffle_check}}', 'store_id');
        $this->addForeignKey('fk_raffle_check_store_id', '{{%raffle_check}}', 'store_id', '{{%raffle_store}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_raffle_check_user_id', '{{%raffle_check}}', 'user_id');
        $this->addForeignKey('fk_raffle_check_user_id', '{{%raffle_check}}', 'store_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_raffle_check_store_id', '{{%raffle_check}}');
        $this->dropForeignKey('fk_raffle_check_user_id', '{{%raffle_check}}');

        $this->dropIndex('ix_raffle_check_store_id', '{{%raffle_check}}');
        $this->dropIndex('ix_raffle_check_user_id', '{{%raffle_check}}');

    }
}
