<?php

use yii\db\Migration;

class m170620_061740_column_is_verify_table_checks extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%raffle_check}}', 'is_verify', $this->smallInteger(1)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%raffle_check}}', 'is_verify');
    }
}
