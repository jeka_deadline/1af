<?php

use yii\db\Migration;

class m170621_083222_alter_table_check extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{raffle_check}}', 'is_verify', 'status');
        $this->addColumn('{{raffle_check}}', 'reason_rejection', $this->text()->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170621_083222_alter_table_check cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170621_083222_alter_table_check cannot be reverted.\n";

        return false;
    }
    */
}
