<?php
namespace backend\modules\core\widgets\UriWidget;

use yii\widgets\InputWidget;
use yii\helpers\Html;

class UriWidget extends InputWidget
{

    public $nameModel;
    public $donorAttribute;

    public function init()
    {
        $this->nameModel = substr($this->nameModel, strrpos($this->nameModel, '\\') + 1);

        $nameDonorField   = $this->nameModel . '\\\\[' . $this->donorAttribute . '\\\\]';
        $nameCurrentField = $this->nameModel . '\\\\[' . $this->attribute . '\\\\]';
        $view             = $this->getView();

        $view->registerJs('$(function() {
            $(document).on("blur", "[name=' . $nameDonorField . ']", function( e ) {
                var donnorField   = $("[name=' . $nameDonorField . '");
                var currentField  = $("[name=' . $nameCurrentField . '");

                if ($(currentField).val() == "") {
                    $(currentField).val(translit($(donnorField).val().toLowerCase()));
                }
            });

            function translit(text){
                // Символ, на который будут заменяться все спецсимволы
                var space = "-"; 
                     
                // Массив для транслитерации
                var transl = {
                    "а": "a", "б": "b", "в": "v", "г": "g", "д": "d", "е": "e", "ё": "e", "ж": "zh", 
                    "з": "z", "и": "i", "й": "j", "к": "k", "л": "l", "м": "m", "н": "n",
                    "о": "o", "п": "p", "р": "r", "с": "s", "т": "t", "у": "u", "ф": "f", "х": "h",
                    "ц": "c", "ч": "ch", "ш": "sh", "щ": "sh", "ъ": space, "ы": "y", "ь": space, "э": "e", "ю": "yu", "я": "ya",
                    " ": space, "_": space, "`": space, "~": space, "!": space, "@": space,
                    "#": space, "$": space, "%": space, "^": space, "&": space, "*": space, 
                    "(": space, ")": space,"-": space, "\=": space, "+": space, "[": space, 
                    "]": space, "\\\\": space, "|": space, "/": space,".": space, ",": space,
                    "{": space, "}": space, "\'": space, "\"": space, ";": space, ":": space,
                    "?": space, "<": space, ">": space, "№":space, "«":space, "»": space
                }
                                
                var result      = "";
                var curent_sim  = "";
                                
                for (i = 0; i < text.length; i++) {
                    // Если символ найден в массиве то меняем его
                    if (transl[text[ i ]] != undefined) {
                        if (curent_sim != transl[ text[ i ] ] || curent_sim != space){
                            result += transl[ text[ i ] ];
                            curent_sim = transl[ text[ i ] ];
                        }                                                                             
                    }
                    // Если нет, то оставляем так как есть
                    else {
                        result += text[i];
                        curent_sim = text[i];
                    }                              
                }

                return result;
            }
        });');
    }

    public function run()
    {
        echo Html::activeTextInput($this->model, $this->attribute, $this->options); 
    }

}