<?php
namespace backend\modules\core\widgets\PrismaNav;

use yii\helpers\Html;
use yii\bootstrap\Dropdown as BootstrapDropdown;

class Dropdown extends BootstrapDropdown
{

    public function init()
    {
        if ($this->submenuOptions === null) {
            // copying of [[options]] kept for BC
            // @todo separate [[submenuOptions]] from [[options]] completely before 2.1 release
            $this->submenuOptions = $this->options;
            unset($this->submenuOptions['id']);
        }
        parent::init();
        Html::removeCssClass($this->options, ['widget' => 'dropdown-menu']);
    }

}