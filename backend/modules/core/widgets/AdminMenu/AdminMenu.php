<?php
namespace backend\modules\core\widgets\AdminMenu;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class AdminMenu extends Widget
{

    public $template = 'default';

    public function run()
    {
        $modules    = include(Yii::getAlias('@backend/config/modules.php'));
        $menuItems  = $displayOrderModules = $defaultDisplayOrderModules = [];

        foreach ($modules as $nameModule => $module) {
            $module = Yii::$app->getModule($nameModule);
            if (isset($module->menuOrder) && !isset($displayOrderModules[ $module->menuOrder ])) {
                $displayOrderModules[ $module->menuOrder ] = $nameModule;
            } else {
                $defaultDisplayOrderModules[] = $nameModule;
            }
        }

        foreach ($defaultDisplayOrderModules as $nameModule) {
            $displayOrderModules[] = $nameModule;
        }

        foreach ($displayOrderModules as $nameModule) {

            $method = 'getMenuItems';
            $module = Yii::$app->getModule($nameModule);

            if (method_exists($module, $method)) {
                $menuItems = ArrayHelper::merge($menuItems, call_user_func([$module, $method]));
            }
        }
        return $this->render($this->template, ['menuItems' => $menuItems]);
    }

}