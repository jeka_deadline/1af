<?php
    use backend\modules\core\widgets\PrismaNav\PrismaNav;
    use backend\modules\core\components\PrismaNavDropDown;
?>

<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">

    <?= PrismaNav::widget([
        'items' => $menuItems,
        'options' => [
            'class' => 'nav nav-pills nav-stacked',
        ],
        'dropDownCaret' => '<i class="fa fa-angle-right drop-icon"></i>',
        'submenuOptions' => [
            'class' => 'submenu',
        ],
        'dropDownOptions' => [
            'class' => 'submenu',
        ],
    ]); ?>

</div>