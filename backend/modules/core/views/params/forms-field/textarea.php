<?php
use yii\helpers\Html;
?>

<div class="form-group">

    <?= Html::label($param->name, NULL, ['class' => 'control-label']); ?>
    
    <?= Html::textarea('Param[' . $param->code . ']', $param->value, ['class' => 'form-control', 'rows' => 5]); ?>

</div>