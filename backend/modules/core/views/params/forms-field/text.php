<?php
use yii\helpers\Html;
?>

<div class="form-group">

    <?= Html::label($param->name, NULL, ['class' => 'control-label']); ?>
    
    <?= Html::textInput('Param[' . $param->code . ']', $param->value, ['class' => 'form-control']); ?>

    <p class="hint"><?= $param->description; ?></p>

</div>