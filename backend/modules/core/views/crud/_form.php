<?php
use yii\helpers\Html;

if ($paramsElement[ 'type' ] == 'image') :
    if (!empty($model->$nameElement)) {
        echo Html::beginTag('div', ['class' => 'row']);
            echo Html::beginTag('div', ['class' => 'col-md-4']);
                echo Html::img('/' . $model->filePath . '/' . $model->$nameElement, ['class' => 'img-responsive']);
            echo Html::endTag('div');
            if (isset($paramsElement[ 'deleteImageUrl' ])) {
                echo Html::beginTag('div', ['class' => 'col-md-4']);
                    echo Html::a(Yii::t('core', 'Delete image'), $paramsElement[ 'deleteImageUrl' ], ['class' => 'btn btn-danger']);
                echo Html::endTag('div');
            }
        echo Html::endTag('div');
    }
endif;

$f = $form->field($model, $nameElement, $fieldAttributes);

// лейбл
if (isset($paramsElement[ 'label' ])) :
    $f->label($paramsElement[ 'label' ]);
endif;

// текст подсказка
if (isset($paramsElement[ 'hint' ])) :
    $f->hint($paramsElement[ 'hint' ]);
endif;

switch (strtolower($paramsElement[ 'type' ])) :

    case 'checkboxlist':
        $f->checkboxList($paramsElement[ 'items' ], $attributes);
        echo $f;
        break;

    case 'dropdownlist':
        $f->dropdownlist($paramsElement[ 'items' ], $attributes);
        echo $f;
        break;

    case 'file':
    case 'image':
        $f->fileInput($attributes);
        echo $f;
        break;

    case 'text':
        $f->textInput($attributes);
        echo $f;
        break;

    case 'widget':
        $f->widget($paramsElement[ 'nameWidget' ], $attributes);
        echo $f;
        break;

    case 'userwidget':
        echo call_user_func([$paramsElement[ 'nameWidget' ], 'widget'], $attributes);
        break;

    default:
        $f->{$paramsElement[ 'type' ]}($attributes);
        echo $f;

endswitch;

?>