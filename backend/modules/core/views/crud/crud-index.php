<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>

<?= $headerContent; ?>

<div class="crud-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($showAddButton) : ?>
      
        <p>
            <?= Html::a(Yii::t('core', "Create"), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

    <?php endif; ?>

    <?= GridView::widget([

        'dataProvider'  => $dataProvider,
        'filterModel'   => $searchModel,
        'columns'       => $columns,

    ]); ?>
    
</div>

<?= $footerContent; ?>