<?php
use yii\widgets\DetailView;
use yii\helpers\Url;
?>
<h2><?= Yii::t('core', 'View support message'); ?></h2>

<a href="<?= Url::toRoute(['/core/support-message/index']); ?>" class="btn btn-default"><?= Yii::t('core', 'Back'); ?></a>
<br>
<br>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => $model->getViewAttributes(),
]); ?>