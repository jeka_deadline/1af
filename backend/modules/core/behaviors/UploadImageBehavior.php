<?php
namespace backend\modules\core\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\imagine\Image;  
use Imagine\Image\Box;

class UploadImageBehavior extends Behavior
{

    public $attribute;
    public $uploadPath;
    public $resizes = [];

    private $_file;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT   => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE   => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_DELETE   => 'deleteFile',
        ];
    }

    public function beforeValidate()
    {
        $model = $this->owner;

        if (($file = $model->getAttribute($this->attribute)) instanceof UploadedFile) {
            $this->_file = $file;
        } else {
            $this->_file = UploadedFile::getInstance($model, $this->attribute);
        }

        if ($this->_file instanceof UploadedFile) {
            $this->_file->name = md5($this->_file->name);
            $model->setAttribute($this->attribute, $this->_file);
        }
    }

    public function beforeSave()
    {
        $model = $this->owner;

        if ($file = UploadedFile::getInstance($model, $this->attribute)) {
            
            if (!is_dir($this->uploadPath)) {
                mkdir($this->uploadPath, 0777, TRUE);
            }

            if ($model->isAttributeChanged($this->attribute)) {
                $this->deleteFile();
            }

            $fileName     = md5($file->baseName . time());
            $fullFileName = $fileName . '.' . $file->extension;
            
            if ($file->saveAs($this->uploadPath . DIRECTORY_SEPARATOR . $fullFileName)) {
                chmod($this->uploadPath . DIRECTORY_SEPARATOR . $fullFileName, 0777);
                $model->{$this->attribute} = $fullFileName;

                foreach ($this->resizes as $itemResize) {
                    $additionalInformation = $itemResize[ 'width' ] . '_' . $itemResize[ 'height' ];

                    Image::thumbnail(
                          $this->uploadPath . DIRECTORY_SEPARATOR . $fullFileName,
                          $itemResize[ 'width' ],
                          $itemResize[ 'height' ]
                    )
                    ->resize(new Box($itemResize[ 'width' ], $itemResize[ 'height' ]))
                    ->save($this->uploadPath . DIRECTORY_SEPARATOR . $fileName . '_' . $additionalInformation . '.' . $file->extension, ['quality' => 70]);

                    chmod($this->uploadPath . DIRECTORY_SEPARATOR . $fileName . '_' . $additionalInformation . '.' . $file->extension, 0777);
                }
            }
        } else {
            if (!$model->isNewRecord) {
                $model->{$this->attribute} = $model->getOldAttribute($this->attribute);
            }
        }
    }

    public function deleteFile()
    {
        $model = $this->owner;
        $file = $this->uploadPath . DIRECTORY_SEPARATOR . $model->getOldAttribute($this->attribute);
        if ($file && is_file($file) && is_writable($file)) {
            unlink($file);
        }

        $filePathInfo = pathinfo($file);

        foreach ($this->resizes as $itemResize) {
            $additionalInformation  = $itemResize[ 'width' ] . '_' . $itemResize[ 'height' ];
            $file                   = $this->uploadPath . DIRECTORY_SEPARATOR . $filePathInfo[ 'filename' ] . '_' . $additionalInformation . '.' . $filePathInfo[ 'extension' ];
            
            if ($file && is_file($file) && is_writable($file)) {
                unlink($file);
            }
        }
    }

}