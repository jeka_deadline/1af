<?php
namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use backend\modules\core\models\searchModels\SupportMessageSearch;
use yii\filters\AccessControl;
use backend\modules\core\models\SupportSubject;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use backend\modules\core\models\Helper;
use yii\filters\VerbFilter;
use backend\modules\core\models\forms\SupportMessageAnswerForm;
use yii\helpers\Html;

class SupportMessageController extends BackendController
{

    public $modelName   = 'backend\modules\core\models\SupportMessage';
    public $searchModel = 'backend\modules\core\models\searchModels\SupportMessageSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => Yii::t('core', 'List support messages'),
                'showAddButton' => FALSE,
            ],
            'delete' => [
                'class' => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function actionView($id)
    {
        $model          = call_user_func_array([$this->modelName, 'findOne'], [$id]);
        $answerModel    = new SupportMessageAnswerForm();
        $footerContent  = (!empty($model->answer)) ? NULL : $this->renderPartial('answer-support-form', [
            'model' => $answerModel,
        ]);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('core', 'Support message not found'));
        }

        if (Yii::$app->request->isPost) {
            if ($answerModel->load(Yii::$app->request->post()) && $answerModel->sendAnswer($model)) {
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        if ($model->is_new) {
            $model->updateAttributes(['is_new' => 0]);
        }

        return $this->render('@app/modules/core/views/crud/crud-view', [
            'headerContent' => FALSE,
            'showUpdateButton' => FALSE,
            'showDeleteButton' => FALSE,
            'model'         => $model,
            'footerContent' => $footerContent,
        ]);
    }

    public function getColumns()
    {
        $listSupportSubjects = ArrayHelper::map(SupportSubject::find()->all(), 'id', 'name');

        return [
            $this->getGridSerialColumn(),
            [
                'attribute' => 'subject_id',
                'value' => function($model){ return $model->subject->name; },
                'filter' => $listSupportSubjects,
            ],
            ['attribute' => 'name'],
            ['attribute' => 'email'],
            ['attribute' => 'text'],
            [
                'attribute' => 'is_new',
                'filter' => Helper::getListYesNo(),
                'value' => function($model){ return $model->getStringIsNew();},
            ],
            [
                'attribute' => 'image',
                'value' => function($model){ return ($model->image) ? Html::a('Смотреть изображение', '/' . call_user_func([$this->modelName, 'getFilePath']) . '/' . $model->image, ['target' => '_blank']) : NULL; },
                'format' => 'raw',
            ],
            $this->getGridActions(['template' => '{view}{delete}']),
        ];
    }

}