<?php
namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;

class AttachmentController extends BackendController
{

    public $modelName   = 'backend\modules\core\models\Attachment';
    public $searchModel = 'backend\modules\core\models\searchModels\AttachmentSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => Yii::t('core', 'List attachments'),
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => Yii::t('core', 'Add attachment'),
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class'     => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute' => 'title'],
            [
                'attribute' => 'file',
                'format'    => 'raw',
                'value'     => function($model)
                {
                    return Html::a($model->file, DIRECTORY_SEPARATOR . call_user_func([$this->modelName, 'getFilePath']) . DIRECTORY_SEPARATOR . $model->file, ['target' => '_blank']);
                }
            ],
            [
                'attribute' => 'file',
                'format'    => 'raw',
                'header'    => Yii::t('core', 'Url'),
                'value'     => function($model)
                {
                    return DIRECTORY_SEPARATOR . call_user_func([$this->modelName, 'getFilePath']) . DIRECTORY_SEPARATOR . $model->file;
                }
            ],
            ['attribute' => 'display_order'],
            $this->getGridActions(['template' => '{delete}']),
        ];
    }

}