<?php
namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class SupportSubjectController extends BackendController
{

    public $modelName   = 'backend\modules\core\models\SupportSubject';
    public $searchModel = 'backend\modules\core\models\searchModels\SupportSubjectSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => 'Список тем сообщений для тех. поддержки',
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => 'Добавить тему сообщения для тех. поддержки',
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'backend\modules\core\components\CRUDUpdate',
                'title'     => 'Обновить тему сообщений тех. поддержки',
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'     => 'backend\modules\core\components\CRUDView',
                'title'     => 'Просмотр темы сообщений тех. поддержки',
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute' => 'name'],
            $this->getGridActive(),
            $this->getGridActions(),
        ];
    }

}