<?php

namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BackendController
{

    public $searchModel = 'backend\modules\core\models\searchModels\PageSearch';
    public $modelName   = 'backend\modules\core\models\Page';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'           => 'backend\modules\core\components\CRUDIndex',
                'title'           =>  Yii::t('core', 'List core pages'),
                'showAddButton'   => FALSE,
            ],
            'update' => [
                'class'           => 'backend\modules\core\components\CRUDUpdate',
                'title'           => Yii::t('core', 'Update core page'),
                'modelName'       => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'header'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(['template' => '{update}']),

        ];
    }

}
