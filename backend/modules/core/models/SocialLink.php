<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\SocialLink as BaseSocialLink;
use yii\helpers\ArrayHelper;

class SocialLink extends BaseSocialLink
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['display_order', 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'title'           => ['type' => 'text'],
            'class'           => ['type' => 'text'],
            'url'             => ['type' => 'text'],
            'display_order'   => ['type' => 'text'],
            'active'          => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'class', 'url', 'display_order', 'active'];
    }

}