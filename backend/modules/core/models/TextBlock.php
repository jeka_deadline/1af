<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\TextBlock as BaseTextBlock;
use yii\helpers\ArrayHelper;

class TextBlock extends BaseTextBlock
{

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['display_order'], 'default', 'value' => 0],
              [['uri'], 'match', 'pattern' => '/^[a-z][a-z\d_-]+[a-z\d]$/i'],
          ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                'title' => Yii::t('core', 'Title text block'),
                'uri' => Yii::t('core', 'URI'),
                'description' => Yii::t('core', 'Description text block'),
                'content' => Yii::t('core', 'Content text block'),
                'display_order' => Yii::t('core', 'Display order'),
                'active' => Yii::t('core', 'Active'),
            ]
        );
    }

    public function getFormElements()
    {
        $fields =  [
            'title'         => ['type' => 'text'],
            'uri'           => ['type' => 'text'],
            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
            'content'       => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'uri', 'description', 'content', 'display_order', 'active'];
    }

}