<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\ReadyAnswerForSupport as BaseReadyAnswerForSupport;
use yii\helpers\ArrayHelper;

class ReadyAnswerForSupport extends BaseReadyAnswerForSupport
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['display_order'], 'default', 'value' => 0],
          ]);
    }

    public function getFormElements()
    {
        $fields =  [
            'header'        => ['type' => 'text'],
            'text'          => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'display_order' => ['type' => 'text'],
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'header', 'text', 'display_order'];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'header' => 'Заголовок ответа',
            'text' => 'Готовый текст ответа',
            'display_order' => Yii::t('core', 'Display order'),
        ];
    }
}
