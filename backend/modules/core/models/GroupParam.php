<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\GroupParam as BaseGroupParam;

class GroupParam extends BaseGroupParam
{

    public function getParams()
    {
        return $this->hasMany(Param::className(), ['group_id' => 'id']);
    }

}