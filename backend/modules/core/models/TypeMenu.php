<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\TypeMenu as BaseTypeMenu;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use backend\modules\page\models\Page as StaticPage;

class TypeMenu extends BaseTypeMenu
{

    public $pages;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['display_order'], 'default', 'value' => 0],
              [['code'], 'match', 'pattern' => '/^[a-z]+$/i'],
              [['pages'], 'safe'],
          ]);
    }

    public function getFormElements()
    {
        $fields =  [
            'title'         =>  ['type' => 'text'],
            'code'          =>  ['type' => 'text'],
            'pages'         =>  [
                                    'type' => 'widget',
                                    'nameWidget' => '\talma\widgets\JsTree',
                                    'attributes' => [
                                        'name' => 'js_tree',
                                        'core' => [
                                            'data' => $this->getDataTree(),
                                        ],
                                    ],
                                ],
            'display_order' =>  ['type' => 'text'],
            'active'        =>  ['type' => 'checkbox'],
        ];

        /*if (!$this->isNewRecord) {
            unset($fields[ 'code' ]);
        }*/

        return $fields;
    }

    public function afterSave($insert, $updateAttributes)
    {
        $this->pages = Json::decode($this->pages);

        $linksMenuPageIds = ArrayHelper::map(LinksMenuPage::find()
                                                                ->where(['type_id' => $this->id])
                                                                ->all(),
                                                'id',
                                                function($model) {return $model; }
                                            );

        $linksCorePages = ArrayHelper::map(LinksMenuPage::find()
                                                              ->where(['type_id' => $this->id, 'type_model' => 'core_page'])
                                                              ->all(),
                                              'page_id',
                                              'id'
                                          );

        $linksStaticPages = ArrayHelper::map(LinksMenuPage::find()
                                                              ->where(['type_id' => $this->id, 'type_model' => 'static_page'])
                                                              ->all(),
                                                'page_id',
                                                'id'
                                            );
        if (!$this->pages) {
            $this->pages = [];
        }

        foreach ($this->pages as $pageTree) {
            $dataItemTree = explode('_', $pageTree);

            if (isset($dataItemTree[ 3 ])) {
                $pageId = $dataItemTree[ 3 ];

                switch ($dataItemTree[ 0 ]) {
                    case 'core':
                        $nameLink = 'linksCorePages';
                        break;
                    case 'static';
                        $nameLink = 'linksStaticPages';
                        break;
                    default:
                        $nameLink = NULL;
                }

                if (!$nameLink) {
                    continue;
                }
                
                if (ArrayHelper::keyExists($pageId, $$nameLink)) {
                    ArrayHelper::remove($linksMenuPageIds, ArrayHelper::getValue($$nameLink, $pageId));
                    //unset($linksMenuPageIds[ ${$nameLink[ $pageId ]}]);
                    continue;
                }

                $link             = new LinksMenuPage();
                $link->type_id    = $this->id;
                $link->page_id    = $pageId;
                $link->type_model = $dataItemTree[ 0 ] . '_page';

                $link->save(FALSE);
            }
        }

        foreach ($linksMenuPageIds as $model) {
            $model->delete();
        }

        parent::afterSave($insert, $updateAttributes);
    }

    public function getViewAttributes()
    {
        return ['id', 'code', 'title', 'display_order', 'active'];
    }

    private function getDataTree()
    {
        $linksCorePages = ArrayHelper::map(LinksMenuPage::find()
                                                              ->where(['type_id' => $this->id, 'type_model' => 'core_page'])
                                                              ->all(),
                                              'page_id',
                                              'id'
                                          );

        $linksStaticPages = ArrayHelper::map(LinksMenuPage::find()
                                                              ->where(['type_id' => $this->id, 'type_model' => 'static_page'])
                                                              ->all(),
                                                'page_id',
                                                'id'
                                            );

        return [
            [
                'id'        => 'core_pages',
                'text'      => 'Страницы системы',
                'children'  => $this->getCorePagesForTree($linksCorePages),
                'state'     => [
                    'opened' => ($linksCorePages) ? TRUE : FALSE,
                ],
            ],
            [
                'id'    => 'static_pages',
                'text'  => 'Статические страницы',
                'children'  => $this->getStaticPagesForTree($linksStaticPages),
                'state'     => [
                    'opened' => ($linksStaticPages) ? TRUE : FALSE,
                ],
            ],
        ];
    }

    private function getCorePagesForTree($linksCorePages)
    {
        $pagesTree = [];

        foreach (Page::find()->all() as $page) {
            $pagesTree[] = [
                'id'    => 'core_page_id_' . $page->id,
                'text'  => $page->header,
                'state' => [
                    'selected' => (isset($linksCorePages[ $page->id ])) ? TRUE : FALSE,
                ],
            ];
        }

        return $pagesTree;
    }

    private function getStaticPagesForTree($linksStaticPages)
    {
        $pagesTree = [];

        foreach (StaticPage::find()->all() as $page) {
            $pagesTree[] = [
                'id'    => 'static_page_id_' . $page->id,
                'text'  => $page->header,
                'state' => [
                    'selected' => (isset($linksStaticPages[ $page->id ])) ? TRUE : FALSE,
                ],
            ];
        }

        return $pagesTree;
    }

}