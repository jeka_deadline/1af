<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\SupportSubject as BaseSupportSubject;
use yii\helpers\ArrayHelper;

class SupportSubject extends BaseSupportSubject
{

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['display_order'], 'default', 'value' => 0],
          ]);
    }

    public function getFormElements()
    {
        $fields =  [
            'name'          => ['type' => 'text'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'display_order', 'active'];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'display_order' => Yii::t('core', 'Display order'),
            'active' => Yii::t('core', 'Active'),
        ];
    }

}