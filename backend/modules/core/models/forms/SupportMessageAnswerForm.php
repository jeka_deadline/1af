<?php
namespace backend\modules\core\models\forms;

use Yii;
use yii\base\Model;
use backend\modules\core\models\SupportMessage;
use yii\helpers\ArrayHelper;
use backend\modules\core\models\ReadyAnswerForSupport;

class SupportMessageAnswerForm extends Model
{

    public $answer;
    public $readyAnswers;

    public function rules()
    {
        return [
            [['answer'], 'required'],
            [['answer'], 'string'],
            [['answer'], 'filter', 'filter' => 'trim'],
        ];
    }

    public function sendAnswer(SupportMessage $model)
    {
        if (!$this->validate()) {
            return FALSE;
        }

        if (Yii::$app->mailer->compose()
                             ->setFrom([Yii::$app->params[ 'supportEmail' ] => 'Support'])
                             ->setTextBody($model->text . "\n" . str_repeat('-', 10) . "\n" . $this->answer)
                             ->setTo($model->email)
                             ->setSubject('Support answer')
                             ->send()) {

            $model->updateAttributes(['answer' => $this->answer]);

            return TRUE;
        }

        return FALSE;
    }

    public function getListReadyAnswers()
    {
        return ArrayHelper::map(ReadyAnswerForSupport::find()->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'header');
    }

    public function attributeLabels()
    {
        return [
            'readyAnswers' => 'Готовые ответы',
            'answer' => 'Ответ',
        ];
    }

}