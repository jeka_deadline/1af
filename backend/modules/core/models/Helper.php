<?php
namespace backend\modules\core\models;

use Yii;

class Helper
{

    public static function getListYesNo()
    {
        return [
            '1' => Yii::t('core', 'Yes'),
            '0' => Yii::t('core', 'No'),
        ];
    }

}