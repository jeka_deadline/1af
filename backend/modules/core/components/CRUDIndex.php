<?php
namespace backend\modules\core\components;

use Yii;
use backend\modules\core\components\BackendBaseAction;

class CRUDIndex extends BackendBaseAction
{

    public $view          = 'crud-index';
    public $showAddButton = TRUE;

    public function run()
    {
        $searchModel  = new $this->controller->searchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->controller->viewPath     = $this->viewPath;
        $this->controller->view->title  = $this->title;

        return $this->controller->render($this->view, [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'columns'       => $this->controller->getColumns(),
            'headerContent' => $this->headerContent,
            'footerContent' => $this->footerContent,
            'showAddButton' => $this->showAddButton,
        ]);

    }

}