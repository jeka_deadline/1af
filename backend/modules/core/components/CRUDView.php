<?php
namespace backend\modules\core\components;

use Yii;
use backend\modules\core\components\BackendBaseAction;

class CRUDView extends BackendBaseAction
{

    public $view              = 'crud-view';
    public $showUpdateButton  = TRUE;
    public $showDeleteButton  = TRUE;
    public $modelPrimaryKey   = 'id';
    public $scenarios         = '';

    public function run()
    {
        $model = $this->findModel(Yii::$app->request->get($this->modelPrimaryKey));

        if (!empty($this->scenarios)) {
            if (is_string($this->scenarios)) {
                $model->setScenario($this->scenarios);
            }
        }

        $this->controller->viewPath     = $this->viewPath;
        $this->controller->view->title  = $this->title;

        return $this->controller->render($this->view, [
            'model'             => $model,
            'showUpdateButton'  => $this->showUpdateButton,
            'showDeleteButton'  => $this->showDeleteButton,
            'headerContent'     => $this->headerContent,
            'footerContent'     => $this->footerContent,
        ]);
    }

}