<?php
namespace backend\modules\core\components;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use common\models\core\AdminLog;

class BackendController extends Controller
{
    protected function getGridSerialColumn()
    {
        return ['class' => 'yii\grid\SerialColumn'];
    }

    protected function getGridActions($options = [])
    {
        $buttons = [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('core', 'Actions'),
        ];

        return ArrayHelper::merge($buttons, $options);
    }

    protected function getGridActive($attribute = 'active')
    {
        return [
            'attribute' => $attribute,
            'filter'    => ['0' => Yii::t('core', 'No'), '1' => Yii::t('core', 'Yes')],
            'value'     => function($model) use ($attribute) {
                              return ($model->$attribute) ? '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>';
            },
            'format'    => 'html',
        ];
    }

    protected function getGridColumnYesNo($attribute)
    {
        return [
            'attribute' => $attribute,
            'filter'    => ['0' => Yii::t('core', 'No'), '1' => Yii::t('core', 'Yes')],
            'value' => function($model){ return ($model->$attribute) ? Yii::t('core', 'Yes') : Yii::t('core', 'No');},
        ];
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $currRequest = Yii::$app->request->pathInfo;
        if ($result && !Yii::$app->user->isGuest) {
            $listDescriptionsUrl = [
                'user/user/index'                   => 'Страница просмотра списка пользователей',
                'raffle/check/index'                => 'Страница просмотра списка чеков',
                'raffle/check/update'               => 'Страница редактирование чека с id = ',
                'raffle/send-status/index'          => 'Cтраница просмотра списка статусов отправки приза',
                'raffle/send-status/delete'         => 'Удаление статуса отправки призов',
                'raffle/send-status/update'         => 'Страница обновления списка статусов отправки приза',
                'raffle/tablet-winner-date/index'   => 'Страница просмотра списка дат розыграша планшетов',
                'raffle/tablet-winner-date/delete'  => 'Удаление даты выиграша планшета',
                'raffle/prize/index'                => 'Страница просмотра списка призов',
                'raffle/prize/view'                 => 'Страница просмотра приза',
                'raffle/prize/update'               => 'Страница обновления приза',
                'raffle/prize/delete-image'         => 'Страница удаления изображения приза',
                'raffle/prize/delete'               => 'Удаление приза',
                'raffle/store/index'                => 'Страница просмотра списка торговых точек',
                'raffle/store/view'                 => 'Страница просмотра торговой точки',
                'raffle/store/update'               => 'Страница обновления торговой точки',
                'raffle/store/delete'               => 'Удаление торговой точки',
                'dictionary/termin/index'           => 'Страница просмотра списка FAQ',
                'dictionary/termin/view'            => 'Страница просмотра элемента FAQ',
                'dictionary/termin/update'          => 'Страница обновления элемента FAQ',
                'dictionary/termin/delete'          => 'Удаление элемента FAQ',
                'dictionary/termin/create'          => 'Страница добавления элемента FAQ',
                'dictionary/category/create'        => 'Страница создания категории FAQ',
                'dictionary/category/index'         => 'Страница просмотра всех категорий FAQ',
                'dictionary/category/update'        => 'Страница обновления категории FAQ',
                'dictionary/category/view'          => 'Страница просмотра категории FAQ',
                'dictionary/category/delete'        => 'Страница удаления категории FAQ',
                'core/support-subject/index'        => 'Страница просмотра списка тем для тех. по',
                'core/support-subject/view'         => 'Страница просмотра темы для тех. по',
                'core/support-subject/update'       => 'Страница обновление темы для тех. по',
                'core/support-subject/delete'       => 'Страница удаление темы для тех. по',
                'core/support-message/index'        => 'Страница просмотра обращений в тех. по',
                'core/support-message/view'         => 'Страница просмотра обращения в тех .по',
                'core/support-message/delete'       => 'Страница удаления обращения в тех.по',
                'user/user/index'                   => 'Страница просмотра списка пользователей',
                'user/user/view'                    => 'Страница просмотра самого пользователя',
                'user/user/update'                  => 'Страница обновления пользователя',
                'user/user/delete'                  => 'Удаление пользователя',
                'user/user/update-user-role'        => 'Обновление роли пользователя',
                'user/user/update-user-block'       => 'Обновление блокировки пользваотеля',
                'page/page/index'                   => 'Страница просмотра статических страниц сайта',
                'page/page/create'                  => 'Страница создания новой статической страницы сайта',
                'page/page/update'                  => 'Страница обновления статической страницы сайта',
                'page/page/view'                    => 'Страница просмотра статической страницы сайта',
                'page/page/delete'                  => 'Страница удаления статической страницы сайта',
                'core/attachment/index'             => 'Страница просмотра списка файлов сайта',
                'core/attachment/create'            => 'Страница загрузки нового файла для сайта',
                'core/attachment/delete'            => 'Страница удаления файла сайта',
                'core/text-block/index'             => 'Страница просмотра списка текстовых блоков сайта',
                'core/text-block/create'            => 'Страница добавления текстового блока сайта',
                'core/text-block/update'            => 'Страница обновления текстового блока сайта',
                'core/text-block/view'              => '',
            ];

            $description = [
                'description'   => (isset($listDescriptionsUrl[ $currRequest ])) ? $listDescriptionsUrl[ $currRequest ] : NULL,
                'POST REQUEST'  => Yii::$app->request->post(),
                'GET REQUEST'   => Yii::$app->request->get(),
            ];

            $description = serialize($description);

            $model              = new AdminLog();
            $model->description = $description;
            $model->user_id     = Yii::$app->user->identity->getId();
            $model->route       = $currRequest;

            $model->save();
        }
        return $result;
    }

}