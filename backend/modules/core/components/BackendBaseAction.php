<?php
namespace backend\modules\core\components;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class BackendBaseAction extends Action
{

    public $modelName;
    public $title         = 'Title';
    public $viewPath      = '@backend/modules/core/views/crud';
    public $headerContent = NULL;
    public $footerContent = NULL;
    public $assets        = NULL;

    public function init()
    {
        parent::init();
        
        if ($this->assets && is_string($this->assets)) {
            $this->assets[ 0 ] = $this->assets;
        }

        if (is_array($this->assets)) {
            foreach ($this->assets as $asset) {
                $asset::register($this->controller->view);
            }
        }
    }

    protected function findModel($params)
    {
        $model = call_user_func(array($this->modelName, 'findOne'), array($params));
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('core', 'The requested page does not exist.'));
        }
    }

}