<?php

use yii\db\Migration;
use yii\db\Expression;

class m170627_120304_admin_log extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%core_admin_log}}', [
            'id' => $this->primaryKey(),
            'description' => $this->text()->defaultValue(NULL),
            'route' => $this->string(255)->notNull(),
            'date' => $this->datetime(),
            'user_id' => $this->integer(11)->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m170627_120304_admin_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170627_120304_admin_log cannot be reverted.\n";

        return false;
    }
    */
}
