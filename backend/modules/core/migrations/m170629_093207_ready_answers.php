<?php

use yii\db\Migration;

class m170629_093207_ready_answers extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%core_ready_answers_for_support}}', [
            'id'            => $this->primaryKey(),
            'header'        => $this->string(100)->notNull(),
            'text'          => $this->text()->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%core_ready_answers_for_support}}');
    }
}
