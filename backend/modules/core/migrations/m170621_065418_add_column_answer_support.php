<?php

use yii\db\Migration;

class m170621_065418_add_column_answer_support extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%core_support_messages}}', 'answer', $this->text()->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170621_065418_add_column_answer_support cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170621_065418_add_column_answer_support cannot be reverted.\n";

        return false;
    }
    */
}
