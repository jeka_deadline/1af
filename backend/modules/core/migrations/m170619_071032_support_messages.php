<?php

use yii\db\Migration;
use yii\db\Expression;

class m170619_071032_support_messages extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%core_support_subjects}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createTable('{{%core_support_messages}}', [
            'id'          => $this->primaryKey(),
            'subject_id'  => $this->integer(11)->notNull(),
            'name'        => $this->string(100)->notNull(),
            'email'       => $this->string(100)->notNull(),
            'text'        => $this->text()->notNull(),
            'image'       => $this->string(255)->defaultValue(NULL),
            'is_new'      => $this->smallInteger(1)->defaultValue(1),
            'date'        => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ]);

        $this->createRelations();
    }

    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%core_support_messages}}');
        $this->dropTable('{{%core_support_subjects}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_core_support_messages_subject_id', '{{%core_support_messages}}', 'subject_id');
        $this->addForeignKey('fk_core_support_messages_subject_id', '{{%core_support_messages}}', 'subject_id', '{{%core_support_subjects}}', 'id', 'CASCADE', 'CASCADE');
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_core_support_messages_subject_id', '{{%core_support_messages}}');
        $this->dropIndex('ix_core_support_messages_subject_id', '{{%core_support_messages}}');
    }
}
