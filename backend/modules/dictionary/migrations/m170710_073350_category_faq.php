<?php

use yii\db\Migration;

class m170710_073350_category_faq extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%dictionary_categories}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(1),
        ]);

        $this->addColumn('{{%dictionary_termins}}', 'category_id', $this->integer(11)->notNull() . ' after id');

        $this->createIndex('ix_dictionary_termins_category_id', '{{%dictionary_termins}}', 'category_id');
        $this->addForeignKey('fk_dictionary_termins_category_id', '{{%dictionary_termins}}', 'category_id', '{{%dictionary_categories}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_dictionary_termins_category_id', '{{%dictionary_termins}}');
        $this->dropIndex('ix_dictionary_termins_category_id', '{{%dictionary_termins}}');

        $this->dropColumn('{{%dictionary_termins}}', 'category_id');
        $this->dropTable('{{%dictionary_categories}}');
    }
}
