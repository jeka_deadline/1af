<?php
namespace backend\modules\dictionary\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TerminController implements the CRUD actions for Termin model.
 */
class TerminController extends BackendController
{

    public $searchModel = 'backend\modules\dictionary\models\searchModels\TerminSearch';
    public $modelName   = 'backend\modules\dictionary\models\Termin';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => Yii::t('dictionary', 'List questions'),
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => Yii::t('dictionary', 'Add question answer'),
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'backend\modules\core\components\CRUDUpdate',
                'title'     => Yii::t('dictionary', 'Update question answer'),
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'     => 'backend\modules\core\components\CRUDView',
                'title'     => Yii::t('dictionary', 'View question answer'),
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class'     => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'name'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
}
