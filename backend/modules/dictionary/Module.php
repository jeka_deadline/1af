<?php
namespace backend\modules\dictionary;

use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    public $controllerNamespace = 'backend\modules\dictionary\controllers';
    public $defaultRoute        = 'index';
    public $menuOrder           = 25;

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations[ 'dictionary' ])) {
            Yii::$app->i18n->translations[ 'dictionary' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/dictionary/messages',
            ];
        }
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => Yii::t('dictionary', 'FAQ'),
                'icon' => '<i class="fa fa-question" aria-hidden="true"></i>',
                'items' => [
                    [
                        'label' => Yii::t('dictionary', 'FAQ categories'),
                        'url'   => ['/dictionary/category/index'],
                    ],
                    [
                        'label' => Yii::t('dictionary', 'FAQ'),
                        'url'   => ['/dictionary/termin/index'],
                    ],
                ],
            ]
        ];
    }
}