<?php
namespace backend\modules\dictionary\models;

use Yii;
use common\models\dictionary\Category as BaseCategory;
use yii\helpers\ArrayHelper;

class Category extends BaseCategory
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'name'          =>  ['type' => 'text'],
            'display_order' =>  ['type' => 'text'],
            'active'        =>  ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'display_order', 'active'];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'display_order' => Yii::t('core', 'Display order'),
            'active' => Yii::t('core', 'Active'),
        ];
    }

}