<?php
    return [
        'Termins'       => 'Словарь терминов',
        'Add termin'    => 'Добавить термин',
        'Update termin' => 'Обновить термин',
        'View termin'   => 'Просмотр термина',
        'List questions' => 'Список ЧаВо вопросов',
        'Add question answer' => 'Добавить вопрос/ответ',
        'View question answer' => 'Вопрос/Ответ',
        'Update question answer' => 'Обновить вопрос/ответ',
        'List categories' => 'Список категорий ЧаВо',
        'Add category' => 'Добавить категорию ЧаВо',
        'Update category' => 'Обновить категорию ЧаВо',
        'View category' => 'Просмотр категории Чаво',
        'FAQ categories' => 'Категории ЧаВо',
    ];
?>