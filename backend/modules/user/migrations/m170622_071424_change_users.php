<?php

use yii\db\Migration;

class m170622_071424_change_users extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%user_users}}', 'full_name', $this->string(100)->defaultValue(NULL));
        $this->alterColumn('{{%user_users}}', 'password_hash', $this->string(100)->defaultValue(NULL));
        $this->alterColumn('{{%user_users}}', 'updated_at', $this->timestamp()->defaultValue(NULL));
        $this->alterColumn('{{%user_users}}', 'confirm_code', $this->string(6)->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170622_071424_change_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170622_071424_change_users cannot be reverted.\n";

        return false;
    }
    */
}
