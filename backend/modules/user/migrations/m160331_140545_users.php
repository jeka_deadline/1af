<?php

use yii\db\Migration;
use yii\db\Expression;

class m160331_140545_users extends Migration
{
    public function safeUp()
    {

      // таблица пользователей
      $this->createTable('{{%user_users}}', [
          'id'                      => $this->primaryKey(),
          'is_admin'                => $this->smallInteger(1)->defaultValue(0),
          'full_name'               => $this->string(100)->notNull(),
          'email'                   => $this->string(100)->defaultValue(NULL),
          'address'                 => $this->text()->defaultValue(NULL),
          'phone'                   => $this->string(50)->notNull(),
          'password_hash'           => $this->string(100)->notNull(),
          'reset_password_token'    => $this->integer()->defaultValue(NULL),
          'auth_key'                => $this->string(32)->notNull(),
          'blocked_at'              => $this->timestamp()->defaultValue(NULL),
          'confirm_code'            => $this->string(32)->defaultValue(NULL),
          'confirm_email_at'        => $this->timestamp()->defaultValue(NULL),
          'confirm_phone_at'        => $this->timestamp()->defaultValue(NULL),
          'register_ip'             => $this->string(15)->notNull(),
          'created_at'              => $this->timestamp()->defaultValue(new Expression('NOW()')),
          'updated_at'              => $this->timestamp()->defaultValue(NULL),
      ]);

      $this->createRelations();
      $this->createAdmins();

    }

    public function safeDown()
    {
        $this->removeRelations();
        $this->dropTable('{{%user_users}}');
    }

    private function createRelations()
    {
        $this->createIndex('uix_user_users_phone', '{{%user_users}}', 'phone', TRUE);
    }

    private function removeRelations()
    {
        $this->dropIndex('uix_user_users_phone', '{{%user_users}}');
    }

    private function createAdmins()
    {
        $this->insert('{{user_users}}', [
            'email'                 => 'jeka.deadline@gmail.com',
            'address'               => 'г. Чернигов',
            'full_name'             => 'Бублик Евгений',
            'is_admin'              => 1,
            'phone'                 => '+380935994767',
            'password_hash'         => \Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'auth_key'              => md5(time() . \Yii::$app->getSecurity()->generateRandomString(50)),
            'confirm_email_at'      => new Expression('NOW()'),
            'confirm_phone_at'      => new Expression('NOW()'),
            'register_ip'           => '127.0.0.1',
        ]);
    }

}
