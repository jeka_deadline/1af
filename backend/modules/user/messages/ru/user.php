<?php
return [
    'Users management' => 'Управление пользователями',
    'Users' => 'Пользователи',
    'Update user' => 'Обновить пользователя',
    'User information' => 'Информация пользователя',
    'Security actions' => 'Дейстия безопасности',
    'Create user' => 'Создать пользователя',
    'Block user' => 'Блокировать пользователя',
    'Unblock user' => 'Разблокировать пользователя',
    'Remove admin role' => 'Удалить права админа',
    'Add admin role' => 'Наделить админ правами',
];
?>