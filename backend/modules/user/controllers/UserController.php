<?php
namespace backend\modules\user\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use backend\modules\user\models\forms\CreateUserForm;
use backend\modules\user\models\User;
use backend\modules\user\models\searchModels\UserSearch;

class UserController extends BackendController
{

    public $modelName   = 'backend\modules\user\models\User';
    public $searchModel = 'backend\modules\user\models\searchModels\UserSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete', 'update-user-role', 'update-user-block'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Список пользователей';

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'currentUserId' => Yii::$app->user->identity->getId(),
        ]);
    }

    public function actionUpdateUserRole($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        if ($user->id === Yii::$app->user->identity->getId()) {
            Yii::$app->session->setFlash('error', Yii::t('user', 'You can not change role for self'));

            return $this->redirect(['index']);
        }

        $user->updateAttributes(['is_admin' => !$user->is_admin]);

        return $this->redirect(['index']);
    }

    public function actionUpdateUserBlock($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        if ($user->id === Yii::$app->user->identity->getId()) {
            Yii::$app->session->setFlash('error', Yii::t('user', 'You can not change block for self'));

            return $this->redirect(['index']);
        }

        $valueBlock = ($user->blocked_at) ? NULL : new Expression('NOW()');

        $user->updateAttributes(['blocked_at' => $valueBlock]);

        return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        if ($user->id === Yii::$app->user->identity->getId()) {
            Yii::$app->session->setFlash('error', Yii::t('user', 'You can not delete self'));

            return $this->redirect(['index']);
        }

        $user->delete();

        Yii::$app->session->setFlash('success', Yii::t('user', 'User success delete'));

        return $this->redirect(['index']);
    }

    public function actionCreate()
    {
        $model = new CreateUserForm(NULL, ['scenario' => CreateUserForm::SCENARIO_CREATE]);

        $this->view->title = 'Добавление пользователя';

        if ($model->load(Yii::$app->request->post()) && $model->createUser()) {
            return $this->redirect(['index']);
        }

        return $this->render('create-update-user-form', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        $this->view->title = 'Обновление пользователя';

        $model = new CreateUserForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->updateUser()) {
            return $this->redirect(['index']);
        }

        return $this->render('create-update-user-form', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException(Yii::t('user', "User not found"));
        }

        $this->view->title = 'Просмотр пользователя';

        return $this->render('view', [
            'model' => $user,
        ]);
    }

}