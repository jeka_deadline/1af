<?php
namespace backend\modules\user\models;

use Yii;
use common\models\user\User as BaseUser;

class User extends BaseUser
{

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_admin' => 'Админ?',
            'full_name' => 'Полное имя',
            'email' => 'Email',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'password_hash' => 'Пароль',
            'reset_password_token' => 'Reset Password Token',
            'auth_key' => 'Auth Key',
            'blocked_at' => 'Заблокирован',
            'confirm_code' => 'Confirm Code',
            'confirm_email_at' => 'Confirm Email At',
            'confirm_phone_at' => 'Confirm Phone At',
            'register_ip' => 'Register Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public static function findByEmailAdmin($email)
    {
        return static::findOne(['email' => $email, 'is_admin' => 1]);
    }

}