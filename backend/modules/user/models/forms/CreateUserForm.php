<?php
namespace backend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use backend\modules\user\models\User;
use backend\modules\user\models\Profile;
use yii\db\Expression;

class CreateUserForm extends Model
{

    // user fields
    public $email;
    public $address;
    public $phone;
    public $password;
    public $repeatPassword;
    public $isAdmin;
    public $isConfirmEmail;
    public $isConfirmPhone;
    public $fullName;

    private $_user;

    const SCENARIO_CREATE = 'create';

    public function __construct($user = NULL, $options = [])
    {
        if ($user instanceof User) {
            $this->_user = $user;

            $this->setFields($user);
        }

        parent::__construct($options);
    }

    public function rules()
    {
        return [
            // user rules
            [['phone', 'fullName'], 'required'],
            [['password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_CREATE],
            [['repeatPassword'], 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_CREATE],
            [['email', 'address', 'phone', 'fullName'], 'filter', 'filter' => 'trim'],
            [['phone'], 'string', 'max' => 50],
            [['email', 'fullName'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['address'], 'string'],
            [['email'], 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email', 'filter' => function($query) {
                if ($this->getScenario() === self::SCENARIO_CREATE) {
                    return $query;
                }

                return $query->andWhere(['<>', 'id', $this->_user->id]);
            }],
            [['phone'], 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'phone', 'filter' => function($query) {
                if ($this->getScenario() === self::SCENARIO_CREATE) {
                    return $query;
                }

                return $query->andWhere(['<>', 'id', $this->_user->id]);
            }],
            [['password', 'repeatPassword'], 'string', 'max' => 100],
            [['isAdmin', 'isConfirmEmail', 'isConfirmPhone'], 'boolean'],
        ];
    }

    public function createUser()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $user = new User();

        $user->email            = $this->email;
        $user->phone            = $this->phone;
        $user->address          = $this->address;
        $user->full_name        = $this->fullName;
        $user->is_admin         = ($this->isAdmin) ? 1 : 0;
        $user->confirm_email_at = ($this->isConfirmEmail) ? new Expression('NOW()') : NULL;
        $user->confirm_phone_at = ($this->isConfirmPhone) ? new Expression('NOW()') : NULL;

        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->setRegisterIp();

        if ($user->validate() && $user->save()) {
            return TRUE;
        }

        return FALSE;
    }

    public function updateUser()
    {
        if (!$this->validate()) {
            print_r($this->getErrors());
        exit;
            return FALSE;
        }

        $user             = $this->_user;
        $user->email      = $this->email;
        $user->phone      = $this->phone;
        $user->full_name  = $this->fullName;
        $user->address    = $this->address;

        if ($this->password) {
            $user->setPassword($this->password);
        }

        if ($user->save()) {
            return TRUE;
        }

        return FALSE;

    }

    public function setFields(User $user)
    {
        $this->email    = $user->email;
        $this->phone    = $user->phone;
        $this->address  = $user->address;
        $this->fullName = $user->full_name;
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'fullName' => 'ФИО',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторить пароль',
            'isAdmin' => 'Наделить правами админа',
            'isConfirmPhone' => 'Потвердить телефон',
            'isConfirmEmail' => 'Подтвердить email',
        ];
    }

}