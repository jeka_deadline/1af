<?php

namespace backend\modules\user\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\User;

/**
 * UserSearch represents the model behind the search form about `backend\modules\user\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_admin', 'reset_password_token'], 'integer'],
            [['full_name', 'email', 'address', 'phone', 'password_hash', 'auth_key', 'blocked_at', 'confirm_code', 'confirm_email_at', 'confirm_phone_at', 'register_ip', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_admin' => $this->is_admin,
            'reset_password_token' => $this->reset_password_token,
            'confirm_email_at' => $this->confirm_email_at,
            'confirm_phone_at' => $this->confirm_phone_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'confirm_code', $this->confirm_code])
            ->andFilterWhere(['like', 'register_ip', $this->register_ip]);

            if ($this->blocked_at !== '') {

                if ((int)$this->blocked_at === 1) {
                    $query->andWhere(['not', ['blocked_at' => NULL]]);
                }
                if ((int)$this->blocked_at === 0) {
                    $query->andWhere(['IS', 'blocked_at', NULL]);
                }
            }

        return $dataProvider;
    }
}
